import { getRepository, getConnection, EntityManager, MoreThan } from "typeorm";
import {
    CrawlStatus,
    NftEntity,
    UserWalletEntity,
    CollectionEntity,
    CollectionActivityEntity,
    UserInfoEntity,
} from "./entities";
import { IEventLogCrawlerOptions, BaseEventLogCrawler } from "./BaseEventLogCrawler";
import { MultiEventLogCrawlerByWeb3 } from "./MultiEventLogCrawlerByWeb3";
import { prepareEnvironment } from "./prepareEnvironment";
import { OwnerNftEntity } from "./entities/OwnerNft";
import { argv } from "process";
import { CollectionActivityTypeEnum, NetworkTypeEnum, SaleStatusEnum, ZERO_ADDRESS } from "./Utils";
import { CrawlerPropertiesQueue } from "./jobs/CrawlerProperties.job";
import { emitNoti } from "./utils/EmitNoti";
const contractName = "collection";
const networkConfig = require(`./configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts[contractName];

// Use in making contract log entity to insert DB

const page = +argv[2];
contractConfig.PAGE = page;

prepareEnvironment()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

function start(): void {
    console.log(`Start crawling event logs of contract: ${contractName}`);

    console.log(contractConfig);

    const crawlerOpts: IEventLogCrawlerOptions = {
        onEventLogCrawled,
        getLatestCrawledBlockNumber,
        networkConfig,
        contractConfig,
    };

    const crawler = new MultiEventLogCrawlerByWeb3(crawlerOpts);
    crawler.start();
}

async function onEventLogCrawled(
    crawler: BaseEventLogCrawler,
    eventLogs: any,
    txId: string,
    eventSeq: number,
    lastProcessedTimestamp: number
): Promise<void> {
    for (const eventLog of eventLogs) {
        await getConnection().manager.transaction("SERIALIZABLE", async (manager) => {
            console.log("LOG EVENT-TRANSACTION", eventLog);

            const eventType = eventLog.type.split("::")[2];

            switch (eventType) {
                case "NFTMinted": {
                    await mintNft(eventLog, manager);
                    break;
                }
                case "NFTTransfer": {
                    await transferNft(eventLog, manager);
                    break;
                }
                case "NFTBurn": {
                    await burnNft(eventLog, manager);
                    break;
                }
            }
        });
    }
}

async function getLatestCrawledBlockNumber(): Promise<{
    txId: string;
    eventSeq: number;
    timestamp: number;
}> {
    const log = await getRepository(CrawlStatus).findOne({ contractName });
    if (!log) {
        return {
            txId: "",
            eventSeq: NaN,
            timestamp: NaN,
        };
    }
    return {
        txId: log.txDigest,
        eventSeq: log.eventSeq,
        timestamp: log.blockTimestamp,
    };
}

async function mintNft(eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    const collectionAddress = event.packageId;

    const collection = await manager.findOne(CollectionEntity, {
        where: { address: collectionAddress },
    });

    if (!collection) {
        console.log("-------------------------STOP-------------------");
        return;
    }

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });
    
    
    const jobOptions = {
        jobId: event.parsedJson.object_id,
        attempts: 3,
        removeOnComplete: 3600,
        removeOnFail: 72 * 3600,
    };
    CrawlerPropertiesQueue.add(
        { nft_address: event.parsedJson.object_id, collection_address: collectionAddress },
        jobOptions
    );

    if (nft) {
        if (!nft.imageUrl) {
            nft.imageUrl = event.parsedJson.img_url;
            await manager.save(nft);
        }
        if (!nft.owner_address) {
            nft.owner_address = event.parsedJson.creator;
            await manager.save(nft);
        }
        // console.log('---------------------1111',nft);

        // return;
    } else {
        const newNft = new NftEntity();
        newNft.nftId = event.parsedJson.object_id;
        newNft.title = event.parsedJson.name;
        newNft.collection_address = collection.address;
        newNft.collectionId = collection.id;
        newNft.imageUrl = event.parsedJson.image_url || event.parsedJson.img_url || "";
        newNft.maxQuantity = 1;
        newNft.owner_address = event.parsedJson.creator;
        newNft.creatorAddress = event.parsedJson.creator;
        newNft.blockTimestamp = time;
        newNft.nftStatus = SaleStatusEnum.CANCEL;

        // console.log("---------------------------------",newNft)

        await manager.save(newNft);

       
    }
    const activity = new CollectionActivityEntity();
    activity.nftAddress = event.parsedJson.object_id;
    activity.collectionId = collection.id;
    activity.transactionId = eventLog.id.txDigest;
    activity.timestamp = time;
    activity.userAddress = event.parsedJson.creator;
    activity.fromAddress = event.parsedJson.creator;
    activity.activity = CollectionActivityTypeEnum.MINT;
    activity.blockTimestamp = +eventLog.timestampMs;
    activity.collectionAddress = collectionAddress;

    await manager.save(activity);

    // await emitNoti("NFTMinted", event);
}

async function transferNft(eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    const collectionAddress = event.packageId;

    const collection = await manager.findOne(CollectionEntity, {
        where: { address: collectionAddress },
    });

    if (!collection) {
        return;
    }

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

    if (!nft) {
        return;
    }

    if (nft) {
        if (nft.blockTimestamp < time) {
            nft.owner_address = event.parsedJson.receiver;
            nft.blockTimestamp = time;
            nft.updatedAt = new Date();
            await manager.save(nft);
        }
    }

    const activity = new CollectionActivityEntity();
    activity.nftAddress = nft.nftId;
    activity.collectionId = collection.id;
    activity.transactionId = eventLog.id.txDigest;
    activity.timestamp = time;
    activity.blockTimestamp = time;
    activity.fromAddress = event.parsedJson.creator;
    activity.userAddress = event.parsedJson.receiver;
    activity.collectionAddress = collection.address;
    activity.activity = CollectionActivityTypeEnum.TRANSFER;

    await manager.save(activity);

    await emitNoti("NFTTransfer", event);
}

async function burnNft(eventLog: any, manager: EntityManager) {
        await getRepository(NftEntity).createQueryBuilder('n')
        .delete()
        .from(NftEntity)
        .where(`nft_address = :nft_address`,{
            nft_address:eventLog.parsedJson?.object_id
        })
        .execute()
   
}
