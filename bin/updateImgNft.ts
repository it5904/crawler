import { getRepository } from "typeorm";
import { prepareEnvironment } from "./prepareEnvironment";
import {  CrawlStatus, NftEntity } from "./entities";
import { getWeb3ProviderLink } from "./Utils";
import { SuiClient } from "@mysten/sui.js/client";

prepareEnvironment()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

async function start() {
  try {      
    const chainUrl = await getWeb3ProviderLink();
    const client = new SuiClient({url: chainUrl.toString()})
    const cursor = await getRepository(CrawlStatus).createQueryBuilder('cs').select(`cs.event_seq`)
    .where( `cs.contract_name = :name`,{
      name:'nft_image_null'
    })
    .getRawOne()
    let newCursor : number = Number(cursor.event_seq)
    const data = await getRepository(NftEntity).createQueryBuilder('n').select('n.nft_address, n.id, n.image_url')
    .where(`n.id > :cursor`,{cursor:cursor.event_seq})
    .getRawMany()
    
    data.map(async(x)=>{
      if (!x.image_url || x.image_url===''){
        const nftInfo = await client.getObject({
          id:x.nft_address,
          options:{
              "showType": true,
              "showOwner": true,
              "showPreviousTransaction": true,
              "showDisplay": true,
              "showContent": true,
              "showBcs": false,
              "showStorageRebate": true
          }
        }
        );
        
        if (nftInfo.error?.code==="deleted"){
          await getRepository(NftEntity).createQueryBuilder('n')
          .delete()
          .from(NftEntity)
          .where(`nft_address = :nft_address`,{
              nft_address:x.nft_address
          })
          .execute()
        }else 
          {
            await getRepository(NftEntity)
            .createQueryBuilder("n")
            .update(NftEntity)
            .set({ imageUrl: (nftInfo.data?.display["data"]["image_url"] || nftInfo.data?.display["data"]["img_url"]) })
            .where(`nft_address=:nft_address`, {
              nft_address: x.nft_address,
            })
            .execute()
          }
        }  
        
      if ( newCursor < x.id) newCursor=Number(x.id)
    })

    await getRepository(CrawlStatus)
          .createQueryBuilder("cs")
          .update(CrawlStatus)
          .set({ eventSeq: newCursor })
          .where(`contract_name=:contract_name`, {
            contract_name: 'nft_image_null',
          })
          .execute()
    
  } catch (error) {
      console.log(error);
  }
}
