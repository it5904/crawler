module.exports = {
    // WEB3_API_URL: process.env.CHAIN_URL.toString(),
    AVERAGE_BLOCK_TIME: 2000,
    REQUIRED_CONFIRMATION: 2,
    contracts: {
        market: {
            CONTRACT_ADDRESS: process.env.MARKET_ADDRESS.toString(), // Update contract address latler
            EVENT_NUM_IN_ONE_GO: 100, // Number of blocks we fetch in one time
            BREAK_TIME_AFTER_ONE_GO: 1000,
            NEED_NOTIFY_BY_WEBHOOK: false, // no need to nofi
        },

        marketv1: {
            CONTRACT_ADDRESS: process.env.MARKET_ADDRESS_V1.toString(), // Update contract address latler
            EVENT_NUM_IN_ONE_GO: 100, // Number of blocks we fetch in one time
            BREAK_TIME_AFTER_ONE_GO: 1000,
            NEED_NOTIFY_BY_WEBHOOK: false, // no need to nofi
        },

        collection: {
            FIRST_CRAWL_BLOCK: Number(process.env.ALLOCATION_FACTORY_START_BLOCK), // Start Block: Block number that contract is created, develop version only
            EVENT_NUM_IN_ONE_GO: 1000, // Number of blocks we fetch in one time
            BREAK_TIME_AFTER_ONE_GO: 1000,
            NEED_NOTIFY_BY_WEBHOOK: false, // no need to nofi,
            NUMBER_CRAWLER_COLLECTION: 5,
            PAGE: 0,
            COLLECTION: [],
        },

        transfer_collection: {
            FIRST_CRAWL_BLOCK: Number(process.env.ALLOCATION_FACTORY_START_BLOCK), // Start Block: Block number that contract is created, develop version only
            EVENT_NUM_IN_ONE_GO: 50, // Number of blocks we fetch in one time
            BREAK_TIME_AFTER_ONE_GO: 1000,
            NEED_NOTIFY_BY_WEBHOOK: false, // no need to nofi,
            NUMBER_CRAWLER_COLLECTION: 5,
            PAGE: 0,
        },
    },
};
