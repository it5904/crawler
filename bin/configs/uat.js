module.exports = {
    // WEB3_API_URL: process.env.CHAIN_URL.toString(),
    AVERAGE_BLOCK_TIME: 10000,
    REQUIRED_CONFIRMATION: 2,
    CHAIN_ID: 4,
    contracts: {
        AllocationFactory: {
            CONTRACT_DATA: require("./contracts/AllocationFactory.json"), // Allocation Factory
            CONTRACT_ADDRESS: process.env.FACTORY_ADDRESS.toString(), // Update contract address latler
            FIRST_CRAWL_BLOCK: Number(process.env.ALLOCATION_FACTORY_START_BLOCK), // Start Block: Block number that contract is created, develop version only
            BLOCK_NUM_IN_ONE_GO: 100, // Number of blocks we fetch in one time
            BREAK_TIME_AFTER_ONE_GO: 5000,
            NEED_NOTIFY_BY_WEBHOOK: false, // no need to nofi
        },

        Allocation: {
            CONTRACT_DATA: require("./contracts/Allocation.json"), // Allocation Factory
            FIRST_CRAWL_BLOCK: Number(process.env.ALLOCATION_FACTORY_START_BLOCK), // Start Block: Block number that contract is created, develop version only
            BLOCK_NUM_IN_ONE_GO: 50, // Number of blocks we fetch in one time
            BREAK_TIME_AFTER_ONE_GO: 500,
            NEED_NOTIFY_BY_WEBHOOK: false, // no need to nofi
        },
    },
};
