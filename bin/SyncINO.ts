import { getRepository } from "typeorm";
import { prepareEnvironment } from "./prepareEnvironment";
import { CollectionEntity, NftEntity } from "./entities";
import PoolINOModel from "./entities/PoolINO.mongo";
import * as Mongoose from "mongoose";
import { getWeb3ProviderLinkProduct } from "./Utils";

prepareEnvironment()
    .then(cronSyncINO)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

async function updateSync(collection: object, listCollection: object[]) {
    try {
        const poolInfo = listCollection.find((x) => x["address"] === collection["address"]);
        const whitelist = poolInfo["whitelist"] || 0;
        const result = await PoolINOModel.findOneAndUpdate(
            { address: collection["address"] },
            { $set: { public: collection["total_items"] - whitelist } },
            { new: true }
        );
        console.log("result: ",result);

        return !!result;

       
    } catch (error) {
        console.log(error);
    }
}

async function cronSyncINO() {
    try {
        const listCollection = await getRepository(CollectionEntity)
            .createQueryBuilder("c")
            .select(`c.address,c.total_items`)
            .where(`c.status = :status OR c.status = :statusFast`, {
                status: 0,
                statusFast: 9,
            })
            .getRawMany();

        // const addressesCollection = listCollection.map((x) => x.c_address);
        // console.log(listCollection);
        await Promise.all([
            listCollection.map(async (x) => {
                await updateSync(x, listCollection);
            }),
        ]);
    } catch (error) {
        console.log(error);
    }
}
