import axios from "axios";
import * as XLSX from "xlsx";

async function importCollection() {
    const workbook = XLSX.readFile("/home/dtc/Documents/sc_collection.ods");
    const sheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[sheetName];
    const data: string[][] = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

    let result: object[] = [];
    data.map((x) => {
        if (x.length > 0) {
            let temp = {
                address: x[0],
                networkType: 1,
                creatorAddress: "test",
                name: x[1],
                logo: x[3],
                bannerImage: x[4],
            };
            result.push(temp);
        }
    });

    result.map(async (x) => {
        await axios.post<Object, any>(
            "https://dev-api.tocen.co/tocen-api/web/collection/admin-create",
            x,
            {
                headers: {
                    Accept: "application/json",
                },
            }
        );
    });
    console.log(result);
}

importCollection();
