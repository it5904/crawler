import { getRepository } from "typeorm";
import { prepareEnvironment } from "./prepareEnvironment";
import {  TocenUserDiscordEntity } from "./entities";
import { Client, GatewayIntentBits } from 'discord.js';

prepareEnvironment()
    .then(updateDiscordUser)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });
    async function addData(): Promise<any> {
    const client: Client = new Client({
        intents: [GatewayIntentBits.GuildMembers],
      });
      try {
        client.on('ready', async () => {
          console.log(`Logged in as ${client.user?.tag}!`);
        //   const guild = client.guilds.resolve('1047783102057037856'); //1047783102057037856.1093058514143952960       
        });
        client.on("guildMemberAdd",async function(member){
            let data :any=await client.users.fetch(member.id)
            data.verified = false
            data.mfaEnabled =false
            data = JSON.parse(JSON.stringify(data))
            console.log(data);
            
            await getRepository(TocenUserDiscordEntity)
                  .createQueryBuilder('d')
                  .insert()
                  .values({
                    avatar: data?.avatar,
                    discordId: data?.id,
                    bot: data?.bot,
                    system: data?.system,
                    username: data?.username,
                    tag: data?.tag,
                    discriminator: data?.discriminator,
                    verified: data?.verified ,
                    mfaEnabled: data?.mfaEnabled ,
                    createdTimestamp: data?.createdTimestamp,
                    defaultAvatarURL: data?.defaultAvatarURL,
                    avatarURL: data?.avatarURL,
                    displayAvatarURL: data?.displayAvatarURL,
                    flags: data?.flags.bitfield,
                  })
                  .execute();
        })
  
        client.on('error', async (e) => {
          console.log('ERROR: ', e);
        });
  
        client.login(
          'MTA5MjcyOTQwNTU2Mzk5NDEzMg.GUITSi.lmNhza789hiNJ4TPsJfnfgO-P33_Hbri1nll7I',
        );
  
        return true;
      } catch (error) {
        console.log(error);
      }
    }

async function updateDiscordUser() {
    try {      
        await addData();
    } catch (error) {
        console.log(error);
    }
}
