import { JobManageEntity } from "./entities";
import { getConnection, getRepository } from "typeorm";
import pm2 from "pm2";
import { CrawlJobStatus } from "./Utils";
import { prepareEnvironment } from "./prepareEnvironment";
import { join } from "path";
const dotenv = require("dotenv");
dotenv.config();

const networkConfig = require(`./configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts.collection;

async function start() {
    pm2.start(
        {
            script: join("./IDO/IDOEventLogCrawler.js"),
            name: `---Product---IDO cralwer `,
        },
        (err) => {
            console.log(err);
            process.exit(1);
        }
    );

    return;
}

prepareEnvironment()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });
