import { NftEntity } from "./../entities/Nft";
import { getRepository } from "typeorm";
import { NftPropertiesEntity } from "../entities/nft-properties.entity";

import { JsonRpcProvider, Connection } from "@mysten/sui.js";
import axios from "axios";
import { prepareEnvironment } from "../prepareEnvironment";
import pLimit from "p-limit";

const limit = pLimit(5);
const https = require("https");
const agent = new https.Agent({
    rejectUnauthorized: false,
});

async function crawlCurrentProperties(
    nft_address: string,
    collection_address: string,
    chainUrl: string
): Promise<any> {
    const connection = new Connection({
        fullnode: chainUrl,
    });

    const provider = new JsonRpcProvider(connection);
    const nftInfo = await provider.getObject({
        id: nft_address,
        options: {
            showType: true,
            showOwner: true,
            showPreviousTransaction: true,
            showDisplay: false,
            showContent: true,
            showBcs: false,
            showStorageRebate: true,
        },
    });

    if (nftInfo["data"]["content"]["fields"]["link"]) {
        console.log(
            `----------------------------${
                nftInfo["data"]["content"]["fields"]["link"]
            }-------------------------`
        );
        const { data } = await axios.get<Object, any>(
            nftInfo["data"]["content"]["fields"]["link"],
            {
                headers: {
                    Accept: "application/json",
                },
                httpsAgent: agent,
            }
        );

        let properties: NftPropertiesEntity[] = [];
        for (let i = 0; i < data.attributes.length; i++) {
            let attributes = data.attributes[i];
            const newPropertiy = new NftPropertiesEntity();
            newPropertiy.nftAddress = nft_address;
            newPropertiy.collectionAddress = collection_address;
            newPropertiy.key = attributes.trait_type;
            newPropertiy.value = attributes.value;

            properties.push(newPropertiy);
        }
        await getRepository(NftPropertiesEntity).save(properties);
    }

    return;
}
async function main() {
    // const data = await getRepository(ChainUrlEntity).find({status:0})
    // let chainUrls = data.map((x) => x.chainUrl);
    let chainUrls = ["https://suinode-testnet.belaunch.io/"];
    let nfts = await getRepository(NftEntity).find();
    for (let i = 0; i < nfts.length; i++) {
        try {
            let chainUrl = chainUrls[Math.floor(Math.random() * chainUrls.length)];
            // const nft = await getRepository(NftPropertiesEntity).findOne({ where: { nftAddress:nfts[i].nftId} });
            // if (nft)
            // {console.log('-------------Already crawl properties of this nft!!!---------------');

            //     continue}
            await crawlCurrentProperties(nfts[i].nftId, nfts[i].collection_address, chainUrl);
        } catch (error) {
            console.log(error);
        }
    }
}
prepareEnvironment()
    .then(main)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });
