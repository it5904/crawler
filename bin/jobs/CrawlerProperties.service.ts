import { getRepository } from "typeorm";
import { Service } from "typedi";
import { JsonRpcProvider, Connection } from "@mysten/sui.js";
import { getWeb3ProviderLink } from "../Utils";
import axios from "axios";
import { NftPropertiesEntity } from "../entities/nft-properties.entity";

@Service()
export class CrawlerPropertiesService {
    async crawlProperties(nft_address: string, collection_address: string): Promise<any> {
        const chainUrl = await getWeb3ProviderLink();
        const connection = new Connection({
            fullnode: chainUrl,
        });

        const provider = new JsonRpcProvider(connection);
        const nftInfo = await provider.getObject({
            id: nft_address,
            options: {
                showType: true,
                showOwner: true,
                showPreviousTransaction: true,
                showDisplay: true,
                showContent: true,
                showBcs: false,
                showStorageRebate: true,
            },
        });

        if ( nftInfo["data"]["content"]["fields"]["link"]||
        nftInfo["data"]["display"]["data"]["link"]) {
            console.log(
                `----------------------------${
                    nftInfo["data"]["content"]["fields"]["link"]
                }-------------------------`
            );
            const { data } = await axios.get<Object, any>(
                nftInfo["data"]["content"]["fields"]["link"]||
                nftInfo["data"]["display"]["data"]["link"],
                {
                    headers: {
                        Accept: "application/json",
                    },
                }
            );
            console.log(JSON.stringify(data));

            let properties: NftPropertiesEntity[] = [];
            if (data.attributes) {
                for (let i = 0; i < data.attributes.length; i++) {
                    let attributes = data.attributes[i];
                    const newPropertiy = new NftPropertiesEntity();
                    newPropertiy.nftAddress = nft_address;
                    newPropertiy.collectionAddress = collection_address;
                    newPropertiy.key = attributes.trait_type;
                    newPropertiy.value = attributes.value;

                    properties.push(newPropertiy);
                }
                await getRepository(NftPropertiesEntity).save(properties);
            }
        }

        return;
    }
}
