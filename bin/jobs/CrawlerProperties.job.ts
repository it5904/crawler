import Queue, { Job } from "bull";
import Container from "typedi";
import { CrawlerPropertiesService } from "./CrawlerProperties.service";

const CrawlerPropertiesQueue = new Queue("CrawlerPropertiesQueue", {
    redis: {
        host: process.env.REDIS_HOST,
        port: +process.env.REDIS_PORT,
        password: process.env.REDIS_PASSWORD,
    },
});

CrawlerPropertiesQueue.process(async (job: Job) => {
    try {
        console.log("------Processing Craw Properties of Nft-Id:------------", job.id);
        const crawlerPropertiesService = Container.get<CrawlerPropertiesService>(
            CrawlerPropertiesService
        );

        const data = job.data;
        await crawlerPropertiesService.crawlProperties(data.nft_address, data.collection_address);
    } catch (e) {
        console.log("ERROR:", e);
        job.moveToFailed(new Error(`ERROR: ${JSON.parse(e)}`), false);
    }
});

export { CrawlerPropertiesQueue };
