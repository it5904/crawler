import  axios  from 'axios';
import { BaseEventLogCrawler } from '../BaseEventLogCrawler';
import { JsonRpcProvider, Connection } from '@mysten/sui.js';
import { implement, override } from 'sota-common';
import _ from 'lodash';
import { getConnection, getRepository } from 'typeorm';
import { CrawlStatus } from '../entities/CrawlStatus';
import { getWeb3ProviderLink } from '../Utils';
import CrawlStatusModel from '../entities/CrawlStatus.mongo';
import { dashToCamelCase } from '../utils/helps';
const { MongoClient } = require("mongodb");


// Store in-progress block
let LATEST_PROCESSED_TIMESTAMP = NaN;
export class StakeEventLogCrawlerByWeb3 extends BaseEventLogCrawler {

  protected async getLatestCrawledBlockNumber(address: string): Promise<{ txId: string, eventSeq: number, timestamp: number }> {
    const log:any = await CrawlStatusModel.findOne({contractAddress:address})
    if (!log) {
      return {
        txId: '',
        eventSeq: NaN,
        timestamp: NaN,
      };
    }
    return {
      txId: log.txDigest,
      eventSeq: log.eventSeq,
      timestamp: log.blockTimestamp,
    };
  }

  @override
  protected async doProcess(): Promise<void> {
    
    this._options.networkConfig.WEB3_API_URL = await getWeb3ProviderLink();

    let latestProcessedTimestamp = LATEST_PROCESSED_TIMESTAMP;
    let txId;
    let eventSeq;
   if (!latestProcessedTimestamp || isNaN(latestProcessedTimestamp) || !txId || !eventSeq || isNaN(eventSeq)) {
      latestProcessedTimestamp = (await this.getOptions().getLatestCrawledBlockNumber()).timestamp;
    }
    
    if (!latestProcessedTimestamp || !txId || !eventSeq) {
      latestProcessedTimestamp = NaN;
      txId = null;
      eventSeq = NaN;
    }

   
    const toBlockTimestamp = await this.processBlocks(txId, eventSeq);

   
    LATEST_PROCESSED_TIMESTAMP = toBlockTimestamp.latestProcessedTimestamp;

    this.setNextTickTimer(60000);

    return;
  }
  /**
   * Process several blocks in one go. Just use single database transaction
   * @param {string} txId - begin of tx cursor
   * @param {number} eventSeq - number of event sequence
   *
   * @returns {number} the highest timestamp that is considered as confirmed
   */
  @implement
  protected async processBlocks(
    txId: string,
    eventSeq: number,
  ): Promise<{
    tx: string,
    seq: number,
    latestProcessedTimestamp: number
  }> {

    try {
      const connection = new Connection({
        fullnode: this.getOptions().networkConfig.WEB3_API_URL.toString(),
      });
      const provider = new JsonRpcProvider(connection);

      const collections = await CrawlStatusModel.find({name:"stake"})
    //   .skip((page < 0 ? 0 : page) * Number(this.getOptions().contractConfig.NUMBER_CRAWLER_COLLECTION))
    //   .limit(this.getOptions().contractConfig.NUMBER_CRAWLER_COLLECTION)    
      console.log(collections);
      
      const filterCollections = collections.filter((collection) => {
        return collection;
      });
      const processedTime = await Promise.all(filterCollections.map(async (collection:any) => {
        const address = collection.contractAddress;
        let cursor;
        let nextCursor;
        const query = {
          MoveModule: {
            package: address,
            module: collection.module,
          }
        };

        const { txId, eventSeq } = await this.getLatestCrawledBlockNumber(address);

        if (txId === '' || txId === null) { cursor = null; }
        else {
          cursor =
          {
            txDigest: txId,
            eventSeq: String(eventSeq) ,
          };
        }

        const updateTotalDeposit = await provider.getObject({
          id: collection.shareObject,
          options: {
              showType: true,
              showOwner: true,
              showPreviousTransaction: true,
              showDisplay: true,
              showContent: true,
              showBcs: false,
              showStorageRebate: true,
          }})

          const url = process.env.DB_CONN_STRING_STAKE;

          const db = process.env.DB_NAME_STAKE || "tocen_stake";
          
          const mongoClient = new MongoClient(url);
          const mongoDB = mongoClient.db(db);
          const projectCollection =  mongoDB.collection("PoolNFT")
          if (updateTotalDeposit.data.content["fields"]["nft_key_stake"]){
            const updateTotalRaise = await projectCollection.findOneAndUpdate(
              {description:"Pool"},
              {$set:{totalNFT:parseInt(updateTotalDeposit?.data?.content["fields"]["nft_key_stake"])}},
              {new:true}
              )
            console.log(11111, updateTotalRaise);
            
          }

          const eventLogs = await provider.queryEvents({
            query:query,
            cursor:cursor,
            limit:100,
            order:'ascending'}
          );

        const events = eventLogs.data;

        const filterEvents = _.filter(events, function (event) {
          // if (event.effects.status.status==="success") { return event; }
          return event
        });

        nextCursor = {
          txDigest: eventLogs.nextCursor.txDigest,
          eventSeq: eventLogs.nextCursor.eventSeq,
        };

        const latestProcessedTimestamp = events.length === 0 ? NaN : Number(events[events.length - 1].timestampMs);

        await this.getOptions().onEventLogCrawled(this, filterEvents, nextCursor?.txDigest, Number(nextCursor?.eventSeq), Number(latestProcessedTimestamp));

        // update latest blocknumber fetched
        if (Number(latestProcessedTimestamp) > 0) {
          console.log(latestProcessedTimestamp,address)
        let crawlStatus :any={}
            crawlStatus.contractAddress = address;
            crawlStatus.isEnable = false;
            crawlStatus.blockTimestamp = latestProcessedTimestamp;
            if (nextCursor?.txDigest ) {
              crawlStatus.txDigest = nextCursor?.txDigest;
              crawlStatus.eventSeq = Number(nextCursor?.eventSeq);
            }
        await CrawlStatusModel.findOneAndUpdate({contractAddress: address},crawlStatus, {new:true, upsert:true})
         
        }

        return latestProcessedTimestamp;

      }));


      return {
        tx: '',
        seq: NaN,
        latestProcessedTimestamp: _.min(processedTime)
      };

    } catch (e) {
      console.log(e);
      const text=`--Product-Stake--${this.getOptions().networkConfig.WEB3_API_URL}\n${e}`
      await axios.get<any>(
          `https://api.telegram.org/bot5854879025:AAFw3S6c2A9sX8Auy4G5mJQ3Ed0xZMxTpWw/sendMessage?chat_id=-859929765&text=${text}`,
          {
            headers: {
              Accept: 'application/json',
            },
          },
        );
    }
  }

  @implement
  protected async getBlockCount(): Promise<number> {
   
    return null
  }
}
