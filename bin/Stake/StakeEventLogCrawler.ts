import { getRepository, getConnection, EntityManager } from "typeorm";
import { CrawlStatus, NftEntity, CollectionEntity, CollectionActivityEntity } from "../entities";
import { IEventLogCrawlerOptions, BaseEventLogCrawler } from "../BaseEventLogCrawler";
import { StakeEventLogCrawlerByWeb3 } from "./StakeEventLogCrawlerByWeb3";
import { prepareEnvironmentStake } from "../prepareEnvironmentStake";
import { CollectionActivityTypeEnum, SaleStatusEnum, getWeb3ProviderLink } from "../Utils";
import { emitNoti } from "../utils/EmitNoti";
import AccountProjectModel, { AccountProjectSchema } from "../entities/AccountProject.mongo";
import CrawlStatusModel from "../entities/CrawlStatus.mongo";
import { Connection, JsonRpcProvider } from "@mysten/sui.js";
import AccountPoolNFTModel from "../entities/AccountPoolNFT.mongo";
const contractName = "collection";
const networkConfig = require(`../configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts[contractName];

prepareEnvironmentStake()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

function start(): void {
    console.log(`Start crawling event logs of contract: ${contractName}`);

    console.log(contractConfig);

    const crawlerOpts: IEventLogCrawlerOptions = {
        onEventLogCrawled,
        getLatestCrawledBlockNumber,
        networkConfig,
        contractConfig,
    };

    const crawler = new StakeEventLogCrawlerByWeb3(crawlerOpts);
    crawler.start();
}

async function onEventLogCrawled(
    crawler: BaseEventLogCrawler,
    eventLogs: any,
    txId: string,
    eventSeq: number,
    lastProcessedTimestamp: number
): Promise<void> {
    const job: any = await CrawlStatusModel.findOne({
        contractAddress: eventLogs[0]?.packageId,
    });
    for (const eventLog of eventLogs) {
        await getConnection().manager.transaction("SERIALIZABLE", async (manager) => {
            // console.log("LOG EVENT-TRANSACTION", eventLog);
            const eventType = eventLog.type.split("::")[2];
            const nameProject: string = job?.name;
            const masterKey = eventLog.parsedJson?.type_nft_stake?.name.split("::")[2]
            
            if (masterKey==="MasterKeyNFT"){
                console.log(eventType,job?.eventStake,job?.eventStake===eventType);
                
                switch (eventType) {
                    case job?.eventStake: {
                        await stake(nameProject, eventLog, manager);
                        break;
                    }
                    case job?.eventClaim: {
                        await unstake(nameProject,eventLog, manager);
                        break;
                    } 
            }}           
        });
    }
}

async function getLatestCrawledBlockNumber(): Promise<{
    txId: string;
    eventSeq: number;
    timestamp: number;
}> {
    // const log = await getRepository(CrawlStatus).findOne({ contractName });
    // if (!log) {
    return {
        txId: "",
        eventSeq: NaN,
        timestamp: NaN,
    };
    // }
    // return {
    //     txId: log.txDigest,
    //     eventSeq: log.eventSeq,
    //     timestamp: log.blockTimestamp,
    // };
}

async function stake(nameProject: string, eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;
    const typeConfig = ["mythical","gold","silver","plain"]

    const chainUrl = await getWeb3ProviderLink();
    console.log(event);
    

    const connection = new Connection({
        fullnode: chainUrl.toString(),
    });
    const provider = new JsonRpcProvider(connection);
    const nftInfo = await provider.getObject({
        id: event.parsedJson.nft_depsoit ,
        options: {
            // showType: true,
            // showOwner: true,
            // showPreviousTransaction: true,
            showDisplay: true,
            // showContent: true,
            // showBcs: false,
            // showStorageRebate: true,
        },
    });

    const image_url = nftInfo?.data?.display?.data["image_url"]
    //"https://ipfs.tocen.co/tocen/ino/masterkey/silver.gif"
    const typeKey = image_url?.split("/")[6].split(".")[0]
    const nftLevel = typeConfig.indexOf(typeKey)+1;
    const nftName = "nft"+ typeKey?.charAt(0).toUpperCase() + typeKey?.slice(1);

    const insertAccPoolNFT = await AccountPoolNFTModel.findOneAndUpdate(
        {nftObjectID:event.parsedJson.nft_depsoit},
        {$set:{
            address:event.parsedJson?.owner_stake || event.sender, 
            createTime: +event.timestampMs,
            nftLevel: nftLevel,
            nftName: nftName,
            nftObjectID: event.parsedJson.nft_depsoit
            }
        },
        {upsert:true, new:true}
    )

}

async function unstake(nameProject: string, eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    const stakedInfo = await AccountPoolNFTModel.findOne({nftObjectID:event.parsedJson.nft_claim})

    if (stakedInfo &&  stakedInfo["createTime"] < time ){
        const removeAccPoolNFT = await AccountPoolNFTModel.deleteOne(
            {nftObjectID:event.parsedJson.nft_claim},
            )
        }

}
