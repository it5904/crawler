import { getRepository, getConnection, EntityManager } from "typeorm";
import { IEventLogCrawlerOptions, BaseEventLogCrawler } from "../BaseEventLogCrawler";
import { ClaimIDOEventLogCrawlerByWeb3 } from "./ClaimIDOEventLogCrawlerByWeb3";
import { prepareEnvironmentStake } from "../prepareEnvironmentStake";
import { CollectionActivityTypeEnum, SaleStatusEnum, getWeb3ProviderLink } from "../Utils";
import AccountProjectModel, { AccountProjectSchema } from "../entities/AccountProject.mongo";
import CrawlStatusModel from "../entities/CrawlStatus.mongo";
import { Connection, JsonRpcProvider } from "@mysten/sui.js";
import AccountPoolNFTModel from "../entities/AccountPoolNFT.mongo";
const contractName = "collection";
const networkConfig = require(`../configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts[contractName];

prepareEnvironmentStake()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

function start(): void {
    console.log(`Start crawling event logs of contract: ${contractName}`);

    console.log(contractConfig);

    const crawlerOpts: IEventLogCrawlerOptions = {
        onEventLogCrawled,
        getLatestCrawledBlockNumber,
        networkConfig,
        contractConfig,
    };

    const crawler = new ClaimIDOEventLogCrawlerByWeb3(crawlerOpts);
    crawler.start();
}

async function onEventLogCrawled(
    crawler: BaseEventLogCrawler,
    eventLogs: any,
    txId: string,
    eventSeq: number,
    lastProcessedTimestamp: number
): Promise<void> {
    const job: any = await CrawlStatusModel.findOne({
        contractAddress: eventLogs[0]?.packageId,
    });
    for (const eventLog of eventLogs) {
        await getConnection().manager.transaction("SERIALIZABLE", async (manager) => {
            const eventType = eventLog.type.split("::")[2];
               
            switch (eventType) {
              
                case job?.eventClaim: {
                    await claim(job,eventLog, manager);
                    break;
                } 
                case job?.eventClaimAirdrop: {
                    await claimAirdrop(job,eventLog, manager);
                    break;
                } 
            }           
        });
    }
}

async function getLatestCrawledBlockNumber(): Promise<{
    txId: string;
    eventSeq: number;
    timestamp: number;
}> {
   
    return {
        txId: "",
        eventSeq: NaN,
        timestamp: NaN,
    };
  
}

async function stake(nameProject: string, eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;
    const typeConfig = ["mythical","gold","silver","plain"]

    const chainUrl = await getWeb3ProviderLink();
    console.log(event);
    

    const connection = new Connection({
        fullnode: chainUrl.toString(),
    });
    const provider = new JsonRpcProvider(connection);
    const nftInfo = await provider.getObject({
        id: event.parsedJson.nft_depsoit ,
        options: {
            // showType: true,
            // showOwner: true,
            // showPreviousTransaction: true,
            showDisplay: true,
            // showContent: true,
            // showBcs: false,
            // showStorageRebate: true,
        },
    });

    const image_url = nftInfo?.data?.display?.data["image_url"]
    //"https://ipfs.tocen.co/tocen/ino/masterkey/silver.gif"
    const typeKey = image_url?.split("/")[6].split(".")[0]
    const nftLevel = typeConfig.indexOf(typeKey)+1;
    const nftName = "nft"+ typeKey?.charAt(0).toUpperCase() + typeKey?.slice(1);

    const insertAccPoolNFT = await AccountPoolNFTModel.findOneAndUpdate(
        {nftObjectID:event.parsedJson.nft_depsoit},
        {$set:{
            address:event.parsedJson?.owner_stake || event.sender, 
            createTime: +event.timestampMs,
            nftLevel: nftLevel,
            nftName: nftName,
            nftObjectID: event.parsedJson.nft_depsoit
            }
        },
        {upsert:true, new:true}
    )

}

async function claim(job: any, eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;
    const code = job.project
    const turn = +eventLog.parsedJson.turn
    let type=""
    if (event.transactionModule=="ido_claim_public"){
        type = "public"
    } else if (event.transactionModule=="ido_claim_private"){
        type = "private"
    }

    const cursor = ".logClaim" + type;
    const isClaim = ".claimed";
    const txnID = ".txnID";
    const claimTime = ".timestamp";

    const dataToUpdate = {
        [code + cursor + ".$" + isClaim]: true,
        [code + cursor + ".$" + claimTime]: time,
        [code + cursor + ".$" + txnID]: event.id.txDigest,
    };

    const query = { address: event.parsedJson.owner_claim, [code + cursor + ".turn"]: turn };
    const response = await AccountProjectModel.findOneAndUpdate(query, {
        $set: dataToUpdate,
    },{new:true});
}

async function claimAirdrop(job: any, eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;
    const code = job.project;
    const isClaim = code + ".isClaimAirdrop";
    const txnID = code + ".claimAirdropTxnID";
    const timestamp = code + ".claimAirdropTime";

    const dataToUpdate = {
        [isClaim]: true,
        [timestamp]: time,
        [txnID]: event.id.txDigest,
    };

    const query = { address: event.parsedJson.owner_claim, [code]: { $exists: true }};
    const response = await AccountProjectModel.findOneAndUpdate(query, {
        $set: dataToUpdate,
    },{new:true});
    
}
