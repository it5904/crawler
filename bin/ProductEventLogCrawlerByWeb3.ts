import  axios  from 'axios';
import { BaseEventLogCrawler } from './BaseEventLogCrawler';
import { implement ,override} from 'sota-common';
import _ from 'lodash';
import { getConnection, getRepository } from 'typeorm';
import { CrawlStatus } from './entities';
import { getWeb3ProviderLink, getWeb3ProviderLinkProduct } from './Utils';
import { SuiClient } from '@mysten/sui.js/client';
import { Client } from 'discord.js';

let LATEST_PROCESSED_TIMESTAMP = NaN;

export class ProductEventLogCrawlerByWeb3 extends BaseEventLogCrawler {

  protected async getLatestCrawledBlockNumber(address: string): Promise<{ txId: string, eventSeq: number, timestamp: number }> {
    const log = await getRepository(CrawlStatus).findOne({ contractAddress: address, contractName: 'marketv1' });
    if (!log) {
      return {
        txId: '',
        eventSeq: NaN,
        timestamp: NaN,
      };
    }
    return {
      txId: log.txDigest,
      eventSeq: log.eventSeq,
      timestamp: log.blockTimestamp,
    };
  }

  @override
  protected async doProcess(): Promise<void> {
    // Firstly try to get latest time  from network
    // const latestNetworkTimestamp = await this.getBlockCount();

    this._options.networkConfig.WEB3_API_URL = await getWeb3ProviderLink();

    // And looking for the latest processed block in local
    let latestProcessedTimestamp = LATEST_PROCESSED_TIMESTAMP;
    let txId;
    let eventSeq;

    // // If there's no data in-process, then try to find it from environment variable
    // if (!latestProcessedTimestamp && this.getOptions().contractConfig.FORCE_CRAWL_TIME) {
    //   latestProcessedTimestamp = parseInt(this.getOptions().contractConfig.FORCE_CRAWL_TIME, 10);
    // }

    // If still no data, use the callback in options to get the initial value for this process
    if (!latestProcessedTimestamp || isNaN(latestProcessedTimestamp) || !txId || !eventSeq || isNaN(eventSeq)) {
      latestProcessedTimestamp = (await this.getOptions().getLatestCrawledBlockNumber()).timestamp;
    }
    // if (!latestProcessedTimestamp && this.getOptions().contractConfig.FIRST_CRAWL_BLOCK) {
    //   latestProcessedTimestamp = parseInt(this.getOptions().contractConfig.FIRST_CRAWL_BLOCK, 10);
    // }

    // If there's no data, just process from the begin
    if (!latestProcessedTimestamp || !txId || !eventSeq) {
      latestProcessedTimestamp = NaN;
      txId = null;
      eventSeq = NaN;
    }

    /**
     * Start with the next time of the latest processed one
     */

    /**
     * If crawled the newest block already
     * Wait for a period that is equal to average block time
     * Then try crawl again (hopefully new block will be available then)
     */


    /**
     * Actual crawl and process blocks
     * about 10 minutes timeout based on speed of gateway
     */
    const toBlockTimestamp = await this.processBlocks(txId, eventSeq);

    /**
     * Cache the latest processed block number
     * Do the loop again in the next tick
     */
    LATEST_PROCESSED_TIMESTAMP = toBlockTimestamp.latestProcessedTimestamp;

    // Otherwise try to continue processing immediately
    this.setNextTickTimer(this.getBreakTimeAfterOneGo());

    return;
  }
  /**
   * Process several blocks in one go. Just use single database transaction
   * @param {string} txId - begin of tx cursor
   * @param {number} eventSeq - number of event sequence
   *
   * @returns {number} the highest timestamp that is considered as confirmed
   */
  @implement
  protected async processBlocks(
    txId: string,
    eventSeq: number,
  ): Promise<{
    tx: string, 
    seq: number,
    latestProcessedTimestamp: number
  }> {

    try {
      const address = this.getOptions().contractConfig.CONTRACT_ADDRESS;
      const client = new SuiClient({url: this.getOptions().networkConfig.WEB3_API_URL.toString()})
      // new Connection({
      //   fullnode: this.getOptions().networkConfig.WEB3_API_URL.toString(),
      // });
      console.log(`Contract ADDRESS: ${JSON.stringify(this.getOptions().contractConfig.CONTRACT_ADDRESS)}`);

      // const provider = new JsonRpcProvider(connection);

      let cursor;
      let nextCursor;
      const query = {
        MoveModule: {
          package: address,
          module: 'tocen_marketplace',
        }
      };

      const { txId, eventSeq, timestamp } = await this.getLatestCrawledBlockNumber(address);

      if (txId === '' || txId === null ) { cursor = null; }
      else { cursor = {
        txDigest: txId,
        eventSeq: String(eventSeq) ,
      };
      }
      // const crawlStatus = await getRepository(CrawlStatus).createQueryBuilder('cs')
      // .select('cs.event_num_in_one_go')
      // .where(`cs.contract_name=:contract_name`,{
      //   contract_name:'marketv1'
      // })
      // .getRawOne()
      const limit = 50
      // Number(crawlStatus?.event_num_in_one_go) || 100

      const eventLogs = await client.queryEvents({
                                      query:query,
                                      cursor:cursor,
                                      limit:limit,
                                      order:'ascending'})
      // await provider.queryEvents({
      //   query:query,
      //   cursor:cursor,
      //   limit:limit,
      //   order:'ascending'}
      // );

      const events = eventLogs.data;

      const filterEvents = _.filter(events, function (event) {
        // if (event.event['moveEvent']) { return event; }
        return event
      });


      if (events.length > 0) {
        const lastEvent = events[events.length - 1];
        nextCursor = {
          txDigest: lastEvent.id.txDigest,
          eventSeq: lastEvent.id.eventSeq,
        };
      }


      const latestProcessedTimestamp = events.length === 0 ? Date.now() : Number(events[events.length - 1].timestampMs);
console.log("----------------------",latestProcessedTimestamp, nextCursor);

      await this.getOptions().onEventLogCrawled(this, filterEvents, nextCursor?.txDigest, Number(nextCursor?.eventSeq), Number(latestProcessedTimestamp));
// update latest blocknumber fetched
if (Number(latestProcessedTimestamp) > 0) {
  let crawlStatus = await getRepository(CrawlStatus).findOne({ contractName: 'marketv1', contractAddress: address });
  if (crawlStatus) {
    crawlStatus.blockTimestamp = Number(latestProcessedTimestamp);
    if (nextCursor?.txDigest ) {
      crawlStatus.txDigest = nextCursor?.txDigest;
      crawlStatus.eventSeq = Number(nextCursor?.eventSeq);
    }
    await getRepository(CrawlStatus).save(crawlStatus);
  } else {
    crawlStatus = new CrawlStatus();
    crawlStatus.contractName = 'marketv1';
    crawlStatus.contractAddress = address;
    crawlStatus.blockTimestamp = Number(latestProcessedTimestamp);
    if (nextCursor?.txDigest ) {
      crawlStatus.txDigest = nextCursor?.txDigest;
      crawlStatus.eventSeq = Number(nextCursor?.eventSeq);
    }
    await getConnection()
      .createQueryBuilder()
      .insert()
      .into(CrawlStatus)
      .values(crawlStatus)
      .execute();
  }
}

// return latestProcessedTimestamp;

;
        return { tx: nextCursor?.txDigest, seq: Number(nextCursor?.eventSeq), latestProcessedTimestamp };
    
    } catch (e) {
      console.log(e);
      const text=`--Product-version1-run1--${this.getOptions().networkConfig.WEB3_API_URL}\n${e}`
      // await axios.get<any>(
      //     `https://api.telegram.org/bot5854879025:AAFw3S6c2A9sX8Auy4G5mJQ3Ed0xZMxTpWw/sendMessage?chat_id=-859929765&text=${text}`,
      //     {
      //       headers: {
      //         Accept: 'application/json',
      //       },
      //     },
      //   );
    }
  }

  @implement
  protected async getBlockCount(): Promise<number> {
  
    return null
  }
}
