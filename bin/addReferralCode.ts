import { prepareEnvironmentStake } from "./prepareEnvironmentStake";
import AccountProfileModel from "./entities/AccountProfile.mongo";

prepareEnvironmentStake()
    .then(addCode)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

function genCode(address: string) {
    var result = "";
    var characters = address;
    var charactersLength = characters.length;
    for (var i = 0; i < 6; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
async function addCode() {
    try {
        const data = await AccountProfileModel.find({ referralCode: { $exists: false } });
        const list = JSON.parse(JSON.stringify(data));
        for (let i = 0; i < list.length; i++) {
            const code = genCode(list[i].address);
            await AccountProfileModel.findOneAndUpdate(
                { address: list[i].address },
                { $set: { referralCode: code } },
                { new: true }
            );
            console.log(code);
        }
    } catch (error) {
        console.log(error);
    }
}
