import { getRepository } from "typeorm";
import { prepareEnvironment } from "./prepareEnvironment";
import { CollectionEntity, TocenUserDiscordEntity } from "./entities";
import { Client, GatewayIntentBits } from 'discord.js';
import pLimit from 'p-limit';

prepareEnvironment()
    .then(updateDiscordUser)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });
    async function getData(idExisted:string[]): Promise<any> {
    const client: Client = new Client({
        intents: [GatewayIntentBits.GuildMembers],
      });
      try {
        client.on('ready', async () => {
          console.log(`Logged in as ${client.user?.tag}!`);
          const guild = client.guilds.resolve('1047783102057037856'); //1047783102057037856.1093058514143952960
          const data2: any = await guild.members.fetch();
          const test = [];
          for (const [key, value] of data2) {
            test.push(JSON.parse(JSON.stringify(value.user)));
                
          }
          const ids = test.map((x)=>x.id)
         
          const result = ids.filter((element) => !idExisted.includes(element));
          const dataAdd = test.filter((x)=>result.includes(x.id))
          const limit = pLimit(5);
          await Promise.all(
            dataAdd.map(async (data) =>
              limit(async () => {
                return await getRepository(TocenUserDiscordEntity)
                  .createQueryBuilder('d')
                  .insert()
                  .values({
                    avatar: data?.avatar,
                    discordId: data?.id,
                    bot: data?.bot,
                    system: data?.system,
                    username: data?.username,
                    tag: data?.tag,
                    discriminator: data?.discriminator,
                    verified: data?.verified,
                    mfaEnabled: data?.mfaEnabled || false,
                    createdTimestamp: data?.createdTimestamp,
                    defaultAvatarURL: data?.defaultAvatarURL,
                    avatarURL: data?.avatarURL,
                    displayAvatarURL: data?.displayAvatarURL,
                    flags: data?.flags,
                  })
                  .execute();
              }),
            ),
          );
        
        });
  
        client.on('error', async (e) => {
          console.log('ERROR: ', e);
        });
  
        client.login(
          'MTA5MjcyOTQwNTU2Mzk5NDEzMg.GUITSi.lmNhza789hiNJ4TPsJfnfgO-P33_Hbri1nll7I',
        );
  
        return true;
      } catch (error) {
        console.log(error);
      }
    }

async function updateDiscordUser() {
    try {
        const listDiscordUser = await getRepository(TocenUserDiscordEntity)
            .createQueryBuilder("c")
            .select(`c.discord_id`)
            .getRawMany();
        const idExisted = listDiscordUser.map((x)=>x.discord_id)
        
        await getData(idExisted);
    } catch (error) {
        console.log(error);
    }
}
