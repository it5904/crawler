import { prepareEnvironmentStake } from "./prepareEnvironmentStake";
const { MongoClient } = require("mongodb");
import * as fs from "fs";

prepareEnvironmentStake()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

async function start() {
    try {
        const url = process.env.DB_CONN_STRING_STAKE;

        const db = process.env.DB_NAME_STAKE || "tocen_stake";

        const mongoClient = new MongoClient(url);
        const mongoDB = mongoClient.db(db);
        const userStakeCollection = mongoDB.collection("userStake");
        const poolStakeCollection = mongoDB.collection("poolStake");
        // const backupStakeCollection = mongoDB.collection("backupStake");
        const userData = await userStakeCollection.find({}).toArray();
        const poolData = await poolStakeCollection.find({}).toArray();

        const date = new Date().toJSON();
        // await backupStakeCollection.insertOne({
        //     timestamp: date,
        //     userData: userData,
        //     poolData: poolData,
        // });
        fs.writeFileSync(
            `/home/LogCollection/userStake_${date}.json`,
            JSON.stringify(userData, null, 2),
            { flag: "w" }
        );
        fs.writeFileSync(
            `/home/LogCollection/poolStake_${date}.json`,
            JSON.stringify(poolData, null, 2),
            { flag: "w" }
        );
    } catch (error) {
        console.log(error);
    }
}
