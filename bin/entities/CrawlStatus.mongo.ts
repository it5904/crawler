// import { IPoolINO } from './pool-ino.interface';
import mongoose from "mongoose";
const Schema = mongoose.Schema;

const CrawlStatusMongoSchema = new Schema(
    {
        name: {
            type: String,
        },
        contractAddress: {
            type: String,
        },
        txDigest: {
            type: String,
        },
        eventSeq: {
            type: String,
        },
        shareObject: {
            type: String,
        },
        functionDeposit: {
            type: String,
        },
        functionDepositPublic: {
            type: String,
        },
        functionDepositPrivate: {
            type: String,
        },
        functionClaim: {
            type: String,
        },
        eventClaim: {
            type: String,
        },
        eventStake: {
            type: String,
        },
        module: {
            type: String,
        },
        functionRefund: {
            type: String,
        },
        blockTimestamp: {
            type: Number,
        },
        isEnable: {
            type: Boolean,
        },
    },
    {
        collection: "CrawlStatus",
    }
);
const CrawlStatusModel = mongoose.model<mongoose.Document>("CrawlStatus", CrawlStatusMongoSchema);
export { CrawlStatusModel, CrawlStatusMongoSchema };
export default CrawlStatusModel;
