// import { SaleTypeEnum, SaleStatusEnum } from "../Utils";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("nfts")
export class NftEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column({
        name: "nft_address",
    })
    nftId: string;

    @Column({
        name: "version",
    })
    version: number;

    @Column({
        name: "collection_id",
    })
    collectionId: number;

    @Column({
        name: "collection_address",
    })
    collection_address: string;

    @Column({
        name: "owner_address",
    })
    owner_address: string;

    @Column({
        name: "listing_price",
    })
    listing_price: string;

    @Column({
        name: "market_price",
    })
    market_price: string;

    @Column({
        name: "offer_price",
    })
    offer_price: string;

    @Column()
    description: string;

    @Column({
        name: "max_quantity",
    })
    maxQuantity: number;

    @Column({
        name: "image_url",
    })
    imageUrl: string;

    @Column({
        name: "creator_address",
    })
    creatorAddress: string;

    @Column({
        type: "jsonb",
        array: false,
        default: () => "'[]'",
        nullable: true,
    })
    properties!: Array<{ key: string; value: string }>;

    @Column({
        name: "external_link",
    })
    externalLink: string;

    @Column({
        name: "expire_time",
        type: "int",
    })
    expireTime: Date;

    @Column({
        name: "created_at",
    })
    createdAt: Date;

    @Column({
        name: "updated_at",
    })
    updatedAt: Date;

    @Column({
        name: "nft_status",
        // type: 'enum',
        // enum: SaleStatusEnum,
        default: 1,
    })
    nftStatus: number;

    @Column({
        name: "start_price",
    })
    startPrice: string;

    @Column({
        name: "end_price",
    })
    endPrice: string;

    @Column({
        name: "price",
    })
    price: string;

    @Column({
        name: "quantity",
    })
    quantity: number;

    @Column({
        name: "sale_type",
        // type: 'enum',
        // enum: SaleTypeEnum,
        default: 0,
    })
    saleType: number;

    @Column({
        name: "reserve_buyer_id",
    })
    reserveBuyer: number;

    @Column({ name: "start_time" })
    startTime: number;

    @Column({ name: "block_timestamp", type: "bigint", nullable: true })
    blockTimestamp: number;
}
