import mongoose from "mongoose";
const Schema = mongoose.Schema;

const AccountPoolNFTMongoSchema = new Schema(
    {
        address: {
            type: String,
        },

        nftObjectID: {
            type: String,
        },
        nftName: {
            type: String,
        },
        nftLevel: {
            type: Number,
        },
        createTime: {
            type: Number,
        },
    },
    {
        collection: "AccountPoolNFT",
    }
);
const AccountPoolNFTModel = mongoose.model<mongoose.Document>(
    "AccountPoolNFT",
    AccountPoolNFTMongoSchema
);
export { AccountPoolNFTModel, AccountPoolNFTMongoSchema };
export default AccountPoolNFTModel;
