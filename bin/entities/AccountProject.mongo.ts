// import { IPoolINO } from './pool-ino.interface';
import mongoose from "mongoose";
const Schema = mongoose.Schema;

const AccountProjectSchema = new Schema(
    {
        address: {
            type: String,
            required: [true, "Please enter an address contract"],
            unique: true,
        },
    },
    {
        collection: "AccountProjects",
    }
);
const AccountProjectModel = mongoose.model<mongoose.Document>(
    "AccountProjects",
    AccountProjectSchema
);
export { AccountProjectModel, AccountProjectSchema };
export default AccountProjectModel;
