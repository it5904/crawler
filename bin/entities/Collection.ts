import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("collections")
export class CollectionEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    address: string;

    @Column()
    name: string;

    @Column()
    description: string;

    @Column({
        name: "banner_image",
    })
    bannerImage: string;

    @Column()
    logo: string;

    @Column({
        name: "network_type",
        // type: "enum",
        // enum: NetworkTypeEnum,
        default: 0,
    })
    networkType: number;

    @Column({
        name: "creator_id",
    })
    creatorId: string;

    @Column({
        name: "collection_type",
        // type: "enum",
        // enum: CollectionTypeEnum,
        default: 0,
    })
    collectionType: number;

    @Column({
        name: "royalty_fee",
    })
    royaltyFee: number;

    @Column({
        name: "discord_url",
    })
    discordUrl: string;

    @Column({
        name: "twitter_url",
    })
    twitterUrl: string;

    @Column({
        name: "status",
        // type: "enum",
        // enum: CollectionStatusEnum,
        default: 0,
    })
    status: number;

    @Column({
        name: "owners",
    })
    owners: number;

    @Column({
        name: "total_items",
    })
    totalItems: number;

    @Column({
        name: "created_at",
    })
    createdAt: Date;

    @Column({
        name: "updated_at",
    })
    updatedAt: Date;
}
