import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tocen_user_discord')
export class TocenUserDiscordEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'discord_id',
  })
  discordId: string;

  @Column()
  bot: boolean;

  @Column()
  system: boolean;

  @Column()
  flags: number;

  @Column()
  username: string;

  @Column()
  avatar: string;

  @Column()
  discriminator: string;

  @Column()
  verified: boolean;

  @Column({
    name: 'mfa_enabled',
  })
  mfaEnabled: boolean;

  @Column({
    name: 'created_timestamp',
  })
  createdTimestamp: number;

  @Column({
    name: 'default_avatar_url',
  })
  defaultAvatarURL: string;

  @Column()
  tag: string;

  @Column({
    name: 'avatar_url',
  })
  avatarURL: string;

  @Column({
    name: 'display_avatar_url',
  })
  displayAvatarURL: string;

  @Column({
    name: 'created_at',
  })
  createdAt: Date;

  @Column({
    name: 'updated_at',
  })
  updatedAt: Date;
}
