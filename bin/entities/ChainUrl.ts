import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("chain-url")
export class ChainUrlEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({
        name: "chain_url",
    })
    public chainUrl: string;

    @Column({
        name: "env",
    })
    public env: string;

    @Column({
        name: "total_txn",
    })
    public totalTxn: number;

    @Column({
        name: "client_status",
    })
    public clientStatus: number;

    @Column({
        name: "response_time",
    })
    public responseTime: number;

    @Column({
        name: "status",
        // type: "enum",
        // enum: ChainUrlStatusEnum,
        default: 0,
    })
    public status: number;

    @Column({
        name: "created_at",
    })
    public createdAt: Date;

    @Column({
        name: "updated_at",
    })
    public updatedAt: Date;
}
