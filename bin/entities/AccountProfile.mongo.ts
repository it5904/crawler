import mongoose from "mongoose";
const Schema = mongoose.Schema;

const AccountProfileMongoSchema = new Schema(
    {
        address: {
            type: String,
        },
        referralCode: {
            type: String,
        },
    },
    {
        collection: "AccountProfile",
    }
);
const AccountProfileModel = mongoose.model<mongoose.Document>(
    "AccountProfile",
    AccountProfileMongoSchema
);
export { AccountProfileModel, AccountProfileMongoSchema };
export default AccountProfileModel;
