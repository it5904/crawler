import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("nft_properties")
export class NftPropertiesEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: "nft_address" })
    nftAddress: string;

    @Column({ name: "collection_address" })
    collectionAddress: string;

    @Column()
    key: string;

    @Column()
    value: string;

    @Column({
        name: "created_at",
    })
    createdAt: Date;

    @Column({
        name: "updated_at",
    })
    updatedAt: Date;
}
