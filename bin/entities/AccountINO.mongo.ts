// import { IPoolINO } from './pool-ino.interface';
import mongoose from "mongoose";
const Schema = mongoose.Schema;

const AccountINOSchema = new Schema(
    {
        address: {
            type: String,
            required: [true, "Please enter an address contract"],
            unique: true,
        },
    },

    {
        collection: "AccountINO",
    }
);
const AccountINOModel = mongoose.model<mongoose.Document>("AccountINO", AccountINOSchema);
export { AccountINOModel, AccountINOSchema };
export default AccountINOModel;
