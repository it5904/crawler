import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
// import { NetworkTypeEnum } from '../Utils';
@Entity("user-wallet")
export class UserWalletEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: "user_id" })
    userInfoId: number;

    @Column({
        name: "network_type",
        // type: 'enum',
        // enum: NetworkTypeEnum,
        default: 0,
    })
    networkType: number;

    @Column()
    address: string;

    @Column({
        name: "created_at",
    })
    createdAt: Date;

    @Column({
        name: "updated_at",
    })
    updatedAt: Date;
}
