import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    BeforeInsert,
    BeforeUpdate,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";

@Entity("crawler-status")
export class CrawlStatus {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: "contract_name" })
    contractName: string;

    @Column({
        name: "contract_address",
    })
    contractAddress: string;

    @Column({
        name: "tx_digest",
    })
    txDigest: string;

    @Column({
        name: "block_timestamp",
    })
    blockTimestamp: number;

    @Column({
        name: "event_num_in_one_go",
    })
    eventNumInOneGo: number;

    @Column({
        name: "module_name",
    })
    moduleName: string;

    @Column({
        name: "mint_function",
    })
    mintFunction: string;

    @Column({
        name: "object_type",
    })
    objectType: string;

    @Column({
        name: "is_enable",
    })
    isEnable: boolean;

    @Column({
        name: "event_seq",
    })
    eventSeq: number;

    @Column({
        name: "mint_event",
    })
    mintEvent: string;

    @Column({
        name: "type",
    })
    type: string;

    @CreateDateColumn({ name: "created_at", type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ name: "updated_at", type: "timestamp" })
    updatedAt: Date;
}
