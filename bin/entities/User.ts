import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("user-info")
export class UserInfoEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    name: string;

    @Column({
        name: "discord_url",
    })
    discordUrl: string;

    @Column({
        name: "update_timestamp",
    })
    updateTimestamp: number;

    @Column({
        name: "twitter_url",
    })
    twitterUrl: string;

    @Column({
        name: "avatar_url",
    })
    avatarUrl: string;

    @Column({
        name: "cover_url",
    })
    coverUrl: string;

    @Column({
        name: "created_at",
    })
    createdAt: Date;

    @Column({
        name: "updated_at",
    })
    updatedAt: Date;
}
