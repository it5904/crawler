import { getRepository, getConnection, EntityManager, MoreThan } from "typeorm";
import {
    CrawlStatus,
    NftEntity,
    UserWalletEntity,
    CollectionEntity,
    CollectionActivityEntity,
} from "./entities";
import { IEventLogCrawlerOptions, BaseEventLogCrawler } from "./BaseEventLogCrawler";
import { prepareEnvironment } from "./prepareEnvironment";
import { argv } from "process";
import {
    CollectionActivityTypeEnum,
    CrawlJobStatus,
    NetworkTypeEnum,
    SaleStatusEnum,
    ZERO_ADDRESS,
    getWeb3ProviderLink,
} from "./Utils";
import { CrawlerPropertiesQueue } from "./jobs/CrawlerProperties.job";
import { MultiEventLogCrawlerByWeb3External } from "./MultiEventLogCrawlerByWeb3External";
import { SuiClient } from "@mysten/sui.js/client";
const contractName = "collection";
const networkConfig = require(`./configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts[contractName];

// Use in making contract log entity to insert DB

const page = +argv[2];
contractConfig.PAGE = page;

prepareEnvironment()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

async function start(): Promise<void> {
    console.log(`Start crawling event logs of contract: ${contractName}`);

    console.log(contractConfig);

    const crawlerOpts: IEventLogCrawlerOptions = {
        onEventLogCrawled,
        getLatestCrawledBlockNumber,
        networkConfig,
        contractConfig,
    };

    const crawler = new MultiEventLogCrawlerByWeb3External(crawlerOpts);
    crawler.start();
}

async function onEventLogCrawled(
    crawler: BaseEventLogCrawler,
    eventLogs: any,
    txId: string,
    eventSeq: number,
    lastProcessedTimestamp: number
): Promise<void> {
    const mintEvent = await getRepository(CrawlStatus).findOne({
        contractAddress: eventLogs[0]?.packageId,
    });
    for (const eventLog of eventLogs) {
        await getConnection().manager.transaction("SERIALIZABLE", async (manager) => {
            console.log("LOG EVENT-TRANSACTION", eventLog);

            const eventType = eventLog.type.split("::")[2];
            // console.log(mintEvent, eventLogs.length);

            switch (eventType) {
                case mintEvent.mintEvent || 'NFTMintedEggs': {
                    await mintNft(eventLog, manager);
                    break;
                }
                case "NFTTransfer": {
                    await transferNft(eventLog, manager);
                    break;
                }
            }
        });
    }
}

async function getLatestCrawledBlockNumber(): Promise<{
    txId: string;
    eventSeq: number;
    timestamp: number;
}> {
    const log = await getRepository(CrawlStatus).findOne({ contractName });
    if (!log) {
        return {
            txId: "",
            eventSeq: NaN,
            timestamp: NaN,
        };
    }
    return {
        txId: log.txDigest,
        eventSeq: log.eventSeq,
        timestamp: log.blockTimestamp,
    };
}

async function mintNft(eventLog: any, manager: EntityManager) {
    const chainUrl = await getWeb3ProviderLink();

    const client = new SuiClient({url: chainUrl.toString()})
    const event = eventLog;
    const time = +eventLog.timestampMs;

    const collectionAddress = event.packageId;

    const collection = await manager.findOne(CollectionEntity, {
        where: { address: collectionAddress },
    });

    if (!collection) {
        console.log("-------------------------STOP-------------------");
        return;
    }

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

    const jobOptions = {
        jobId: event.parsedJson.object_id,
        attempts: 3,
        removeOnComplete: 3600,
        removeOnFail: 72 * 3600,
    };
    CrawlerPropertiesQueue.add(
        { nft_address: event.parsedJson.object_id || event.parsedJson.object, collection_address: collectionAddress },
        jobOptions
    );

    if (nft) {
    } else {
        const nftInfo = await client.getObject({
            id: event.parsedJson.object_id || event.parsedJson.id || event.parsedJson.object || event.parsedJson.nft_ids[0],
            options: {
                // showType: true,
                // showOwner: true,
                // showPreviousTransaction: true,
                showDisplay: true,
                // showContent: true,
                // showBcs: false,
                // showStorageRebate: true,
            },
        });

        const newNft = new NftEntity();
        newNft.nftId = event.parsedJson.object_id ||event.parsedJson.id || event.parsedJson.object || event.parsedJson.nft_ids[0];
        newNft.title =nftInfo?.data?.display?.data["name"]|| event.parsedJson.name ||nftInfo?.data?.display?.data["description"];
        newNft.collection_address = collection.address;
        newNft.collectionId = collection.id;
        newNft.imageUrl =
            nftInfo?.data?.display?.data["image_url"] || nftInfo?.data?.display?.data["img_url"];
        newNft.maxQuantity = 1;
        newNft.owner_address = event.parsedJson.creator  || event.sender ;
        newNft.creatorAddress = event.parsedJson.creator||event.sender;
        newNft.blockTimestamp = time;
        newNft.nftStatus = SaleStatusEnum.CANCEL;

        await manager.save(newNft);
        const activity = new CollectionActivityEntity();
        activity.nftAddress = event.parsedJson.object_id||event.parsedJson.id || event.parsedJson.object;
        activity.collectionId = collection.id;
        activity.transactionId = eventLog.id.txDigest;
        activity.timestamp = time;
        activity.userAddress =  event.parsedJson.creator||event.sender;
        activity.fromAddress =  event.parsedJson.creator||event.sender;
        activity.activity = CollectionActivityTypeEnum.MINT;
        activity.blockTimestamp = +eventLog.timestampMs;
        activity.collectionAddress = collectionAddress;
    
        await manager.save(activity);
    }
}

async function transferNft(eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    const collectionAddress = event.packageId;

    const collection = await manager.findOne(CollectionEntity, {
        where: { address: collectionAddress },
    });

    if (!collection) {
        return;
    }

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

    if (!nft) {
        return;
    }

    if (nft) {
        if (nft.blockTimestamp < time) {
            nft.owner_address = event.parsedJson.receiver;
            nft.blockTimestamp = time;
            nft.updatedAt = new Date();
            await manager.save(nft);
        }
    }

    const activity = new CollectionActivityEntity();
    activity.nftAddress = nft.nftId;
    activity.collectionId = collection.id;
    activity.transactionId = eventLog.id.txDigest;
    activity.timestamp = time;
    activity.blockTimestamp = time;
    activity.fromAddress = event.parsedJson.creator;
    activity.userAddress = event.parsedJson.receiver;
    activity.collectionAddress = collection.address;
    activity.activity = CollectionActivityTypeEnum.TRANSFER;

    await manager.save(activity);
}
