import { getRepository, getConnection, EntityManager } from "typeorm";
import { CrawlStatus, NftEntity, CollectionEntity, CollectionActivityEntity } from "../entities";
import { IEventLogCrawlerOptions, BaseEventLogCrawler } from "../BaseEventLogCrawler";
import { IDOEventLogCrawlerByWeb3 } from "./IDOEventLogCrawlerByWeb3";
import { prepareEnvironmentStake } from "../prepareEnvironmentStake";
import { CollectionActivityTypeEnum, SaleStatusEnum } from "../Utils";
import { emitNoti } from "../utils/EmitNoti";
import AccountProjectModel, { AccountProjectSchema } from "../entities/AccountProject.mongo";
import CrawlStatusModel from "../entities/CrawlStatus.mongo";
const contractName = "collection";
const networkConfig = require(`../configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts[contractName];

prepareEnvironmentStake()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

function start(): void {
    console.log(`Start crawling event logs of contract: ${contractName}`);

    console.log(contractConfig);

    const crawlerOpts: IEventLogCrawlerOptions = {
        onEventLogCrawled,
        getLatestCrawledBlockNumber,
        networkConfig,
        contractConfig,
    };

    const crawler = new IDOEventLogCrawlerByWeb3(crawlerOpts);
    crawler.start();
}

async function onEventLogCrawled(
    crawler: BaseEventLogCrawler,
    eventLogs: any,
    txId: string,
    eventSeq: number,
    lastProcessedTimestamp: number
): Promise<void> {
    const job: any = await CrawlStatusModel.findOne({
        isEnable: true,
        contractAddress: eventLogs[0]?.transaction.data.transaction.transactions[1]?.MoveCall.package,
    });
    for (const eventLog of eventLogs) {
        await getConnection().manager.transaction("SERIALIZABLE", async (manager) => {
            // console.log("LOG EVENT-TRANSACTION", eventLog);
            const functionType =
                eventLog.transaction.data.transaction.transactions[1]?.MoveCall.function;
            const nameProject: string = job?.name;

            switch (functionType) {
                case job?.functionDeposit: {
                    await buyCoin(nameProject, eventLog, manager);
                    break;
                }
                case job?.functionRefund:
                case job?.functionClaim: {
                    await claimCoin(nameProject,eventLog, manager);
                    break;
                }
               
            }
        });
    }
}

async function getLatestCrawledBlockNumber(): Promise<{
    txId: string;
    eventSeq: number;
    timestamp: number;
}> {
    // const log = await getRepository(CrawlStatus).findOne({ contractName });
    // if (!log) {
    return {
        txId: "",
        eventSeq: NaN,
        timestamp: NaN,
    };
    // }
    // return {
    //     txId: log.txDigest,
    //     eventSeq: log.eventSeq,
    //     timestamp: log.blockTimestamp,
    // };
}

async function buyCoin(nameProject: string, eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    AccountProjectSchema.add({
        [nameProject]: {
            type: Object,
            required: false,
            unique: false,
        },
    });

    const deposit = nameProject + ".deposit";
    const depositTime = nameProject + ".depositTime";
    const depositTxnID = nameProject + ".depositTxnID";

    const data = await AccountProjectModel.findOneAndUpdate(
        { address: event.transaction.data.sender },
        {
            $set: {
                [deposit]: +event.transaction.data.transaction.inputs[0].value,
                [depositTime]: time,
                [depositTxnID]: event.digest,
            },
        },
        { new: true, upsert: true }
    );
}

async function claimCoin(nameProject: string, eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    AccountProjectSchema.add({
        [nameProject]: {
            type: Object,
            required: false,
            unique: false,
        },
    });

    const isClaim = nameProject + ".isClaim";
    const claimTime = nameProject + ".claimTime";
    const claimTxnID = nameProject + ".claimTxnID";

    const data = await AccountProjectModel.findOneAndUpdate(
        { address: event.transaction.data.sender },
        {
            $set: {
                [isClaim]: true,
                [claimTime]: time,
                [claimTxnID]: event.digest,
            },
        },
        { new: true, upsert: true }
    );
}
