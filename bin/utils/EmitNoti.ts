import axios from "axios";
const dotenv = require('dotenv');
dotenv.config();
export async function emitNoti(event: string, data: any) {
    await axios.post(
        process.env.NOTI_URL,
        { event: event, metadata: data },
        {
            headers: {
                "Content-Type": "application/json",
                "x-api-key": process.env.NOTI_API_KEY,
            },
        }
    );
}
