const dotenv = require('dotenv');
dotenv.config();
export function dashToCamelCase(myStr :string, isUpperFirst = false) {
    if (typeof myStr != "string" || !myStr) return "";
    let str = myStr.replace(/-([a-z])/g, function (g) {
        return g[1].toUpperCase();
    });
    return isUpperFirst ? capitalizeFirstLetter(str) : str;
}

function capitalizeFirstLetter(string: string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}