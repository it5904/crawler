import { getRepository } from "typeorm";
import { prepareEnvironment } from "./prepareEnvironment";
import axios from "axios";
import { ChainUrlEntity } from "./entities";

async function getTimeResponse(url: string) {
    let time = performance.now();
    const body = {
        jsonrpc: "2.0",
        id: 1,
        method: "suix_queryEvents",
        params: [
            {
                MoveModule: {
                    package: "0xa15fb2d14a60b2fa6f32ae20e9f821447fa688f69ac0231819bcd7f7cbab8259",
                    module: "collection",
                },
            },
            {
                txDigest: "4XCFoGmjEZxYquJDBdyJt7MgvEFv82vBLPbyDogPaEsZ",
                eventSeq: "0",
            },
            null,
            false,
        ],
    };

   
    
    await axios
        .post(url, body)
        .then(async (response) => {
            const resTime = parseInt((performance.now() - time).toFixed(0));
            console.log(resTime, url);
            if (resTime < 5000) {
                await getRepository(ChainUrlEntity)
                    .createQueryBuilder("cu")
                    .update(ChainUrlEntity)
                    .set({ responseTime: resTime, clientStatus: 0 })
                    .where(`chain_url=:chain_url`, {
                        chain_url: url,
                    })
                    .execute();
            } else {
                await getRepository(ChainUrlEntity)
                    .createQueryBuilder("cu")
                    .update(ChainUrlEntity)
                    .set({ responseTime: resTime })
                    .where(`chain_url=:chain_url`, {
                        chain_url: url,
                    })
                    .execute();
            }
        })
        .catch(async (err) => {
            console.log(err);
            await getRepository(ChainUrlEntity)
                .createQueryBuilder("cu")
                .update(ChainUrlEntity)
                .set({ clientStatus: 1, responseTime: 0 })
                .where(`chain_url=:chain_url`, {
                    chain_url: url,
                })
                .execute();
        });

    //check if all unable
    // const listChainUrl = await getRepository(ChainUrlEntity)
    //     .createQueryBuilder("cu")
    //     .select(`chain_url`)
    //     .where(`client_status=:clientStatus`, {
    //         clientStatus: 0,
    //     })
    //     .getRawMany();
}

async function getTotalTransaction(url: string) {

    const bodyTotalTxn = {
        jsonrpc: "2.0",
        id: 1,
        method: "sui_getTotalTransactionBlocks",
        params: [],
    };

    await axios
        .post(url, bodyTotalTxn)
        .then(async (response) => {
            console.log(response?.data?.result);

            await getRepository(ChainUrlEntity)
                .createQueryBuilder("cu")
                .update(ChainUrlEntity)
                .set({ totalTxn: response?.data?.result })
                .where(`chain_url=:chain_url`, {
                    chain_url: url,
                })
                .execute();
        })
        .catch(async (err) => {
            console.log(err);
        });

}

prepareEnvironment()
    .then(CheckHealthChainUrl)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

async function CheckHealthChainUrl() {
    try {
        let listUrl = await getRepository(ChainUrlEntity)
            .createQueryBuilder("cu")
            .select(`cu.chain_url`)
            .where(`cu.client_status >= 0`)
            .getRawMany();
        await Promise.allSettled(
            listUrl.map(async (x) => {
                await getTimeResponse(x.chain_url);
            })
        );

        let listAllUrl = await getRepository(ChainUrlEntity)
        .createQueryBuilder("cu")
        .select(`cu.chain_url`)
        .getRawMany();
         if (listAllUrl){
            await Promise.allSettled(
                listAllUrl.map(async (x) => {
                    await getTotalTransaction(x.chain_url);
                })
            );
         }
    } catch (error) {
        console.log(error);
    }
}
