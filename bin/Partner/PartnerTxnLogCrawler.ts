import { getRepository, getConnection, EntityManager } from "typeorm";
import { CrawlStatus, NftEntity, CollectionEntity, CollectionActivityEntity } from "../entities";
import { IEventLogCrawlerOptions, BaseEventLogCrawler } from "../BaseEventLogCrawler";
import { PartnerTxnLogCrawlerByWeb3 } from "./PartnerTxnLogCrawlerByWeb3";
import { prepareEnvironment } from "../prepareEnvironment";
import AccountProjectModel, { AccountProjectSchema } from "../entities/AccountProject.mongo";
import CrawlStatusModel from "../entities/CrawlStatus.mongo";
import { CollectionActivityTypeEnum } from "../Utils";
import { SaleStatusEnum } from "../Utils";
const contractName = "collection";
const networkConfig = require(`../configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts[contractName];

prepareEnvironment()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

function start(): void {
    console.log(`Start crawling event logs of contract: ${contractName}`);

    console.log(contractConfig);

    const crawlerOpts: IEventLogCrawlerOptions = {
        onEventLogCrawled,
        getLatestCrawledBlockNumber,
        networkConfig,
        contractConfig,
    };

    const crawler = new PartnerTxnLogCrawlerByWeb3(crawlerOpts);
    crawler.start();
}

async function onEventLogCrawled(
    crawler: BaseEventLogCrawler,
    eventLogs: any,
    txId: string,
    eventSeq: number,
    lastProcessedTimestamp: number
): Promise<void> {
    for (const eventLog of eventLogs) {
        await getConnection().manager.transaction("SERIALIZABLE", async (manager) => {
            console.log("LOG TRANSACTION:  ", eventLog);
            if (eventLog.content?.fields?.image_url || eventLog.content?.fields?.img_url 
                || eventLog.display?.data?.image_url || eventLog.display?.data?.img_url)
                {
                    await mintNft(eventLog, manager);
                }
        });
    }
}

async function getLatestCrawledBlockNumber(): Promise<{
    txId: string;
    eventSeq: number;
    timestamp: number;
}> {
    // const log = await getRepository(CrawlStatus).findOne({ contractName });
    // if (!log) {
    return {
        txId: "",
        eventSeq: NaN,
        timestamp: NaN,
    };
    // }
    // return {
    //     txId: log.txDigest,
    //     eventSeq: log.eventSeq,
    //     timestamp: log.blockTimestamp,
    // };
}
async function mintNft(eventLog: any, manager: EntityManager) {
    console.log(22222222222, eventLog);
    
    const event = eventLog;
    const time = +eventLog.timestamp;

    const collectionAddress = eventLog.collectionAddress;

    const collection = await manager.findOne(CollectionEntity, {
        where: { address: collectionAddress },
    });

    if (!collection) {
        console.log(collectionAddress);
        
        console.log("-------------------------STOP-------------------");
        return;
    }

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.objectId } });

    if (nft) {
       
        // console.log('---------------------1111',nft);

        // return;
    } else {
        const newNft = new NftEntity();
        newNft.nftId = event.objectId;
        newNft.title = event.content?.fields?.name;
        newNft.collection_address = collection?.address;
        newNft.collectionId = collection?.id;
        newNft.imageUrl = eventLog.content?.fields?.image_url || eventLog.content?.fields?.img_url 
        || eventLog.display?.data?.image_url || eventLog.display?.data?.img_url ;
        newNft.maxQuantity = 1;
        newNft.owner_address = event.owner?.AddressOwner || event.owner?.ObjectOwner
        newNft.creatorAddress = event.owner?.AddressOwner || event.owner?.ObjectOwner
        newNft.blockTimestamp = time || 0;
        newNft.updatedAt = new Date();
        newNft.nftStatus = SaleStatusEnum.CANCEL;

        // console.log("---------------------------------",newNft)

        await manager.save(newNft);
    }
    const activity = new CollectionActivityEntity();
    activity.nftAddress = event.objectId;
    activity.collectionId = collection?.id;
    activity.transactionId = eventLog.digest;
    activity.timestamp = +time || 0;
    activity.userAddress = event.owner?.AddressOwner || event.owner?.ObjectOwner
    activity.fromAddress = event.owner?.AddressOwner || event.owner?.ObjectOwner
    activity.activity = CollectionActivityTypeEnum.MINT;
    activity.blockTimestamp = +time || 0;
    activity.collectionAddress = collectionAddress;

    await manager.save(activity);
}
