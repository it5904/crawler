import  axios  from 'axios';
import { BaseEventLogCrawler } from '../BaseEventLogCrawler';
import { implement, override } from 'sota-common';
import _, { create } from 'lodash';
import { getConnection, getRepository } from 'typeorm';
import { CrawlStatus } from '../entities/CrawlStatus';
import { getWeb3ProviderLink } from '../Utils';
import { SuiClient } from '@mysten/sui.js/client';

// Store in-progress block
let LATEST_PROCESSED_TIMESTAMP = NaN;
let TxID;
const EVENTSEQ = NaN;
export class PartnerTxnLogCrawlerByWeb3 extends BaseEventLogCrawler {

  protected async getLatestCrawledBlockNumber(address: string): Promise<{ txId: string, eventSeq: number, timestamp: number }> {
    const log = await getRepository(CrawlStatus).findOne({ contractAddress: address, contractName: 'collection', type:'partner' });
    if (!log) {
      return {
        txId: '',
        eventSeq: NaN,
        timestamp: NaN,
      };
    }
    return {
      txId: log.txDigest,
      eventSeq: log.eventSeq,
      timestamp: log.blockTimestamp,
    };
  }

  @override
  protected async doProcess(): Promise<void> {
   
    this._options.networkConfig.WEB3_API_URL = await getWeb3ProviderLink();

    let latestProcessedTimestamp = LATEST_PROCESSED_TIMESTAMP;
    let txId;
    let eventSeq;

  
    if (!latestProcessedTimestamp || isNaN(latestProcessedTimestamp) || !txId || !eventSeq || isNaN(eventSeq)) {
      latestProcessedTimestamp = (await this.getOptions().getLatestCrawledBlockNumber()).timestamp;
    }
   
    if (!latestProcessedTimestamp || !txId || !eventSeq) {
      latestProcessedTimestamp = NaN;
      txId = null;
      eventSeq = NaN;
    }

   
    const toBlockTimestamp = await this.processBlocks(txId, eventSeq);

   
    LATEST_PROCESSED_TIMESTAMP = toBlockTimestamp.latestProcessedTimestamp;

    
    this.setNextTickTimer(this.getBreakTimeAfterOneGo());

    return;
  }
  /**
   * Process several blocks in one go. Just use single database transaction
   * @param {string} txId - begin of tx cursor
   * @param {number} eventSeq - number of event sequence
   *
   * @returns {number} the highest timestamp that is considered as confirmed
   */
  @implement
  protected async processBlocks(
    txId: string,
    eventSeq: number,
  ): Promise<{
    tx: string,
    seq: number,
    latestProcessedTimestamp: number
  }> {

    try {
      const page = Number(this.getOptions().contractConfig.PAGE || 1) - 1;
      const client = new SuiClient({url: this.getOptions().networkConfig.WEB3_API_URL.toString()})

      const collections = await getRepository(CrawlStatus).createQueryBuilder('c')
        .where(`c.contract_name = :name AND type = :type`, {
          name: 'collection',
          type: 'partner'
        })
        .limit(this.getOptions().contractConfig.NUMBER_CRAWLER_COLLECTION)
        .offset((page < 0 ? 0 : page) * Number(this.getOptions().contractConfig.NUMBER_CRAWLER_COLLECTION))
        .orderBy('c.created_at')
        .getMany();

      const filterCollections = collections.filter((collection) => {
        if(collection.isEnable) return collection;
      });
      const processedTime = await Promise.all(filterCollections.map(async (collection) => {
        const address = collection.contractAddress;
        let cursor;
        let nextCursor;
        const filter = {
            InputObject:address      
        };

        const { txId, eventSeq, timestamp } = await this.getLatestCrawledBlockNumber(address);

        if (txId === '' || txId === null) { cursor = null; }
        else 
        {
          cursor =txId
      
        }
        const limit = Number(collection.eventNumInOneGo)

        const eventLogs = await client.queryTransactionBlocks({
          filter:filter,
          options:{
                showInput:true,
                showEffects:true,
                showEvents:true,
                showObjectChanges:true,
                showBalanceChanges:false,
             },
          cursor:cursor,
          limit:limit,
          order:'ascending'
        }
        );

        const events = eventLogs.data;
        const mintFunctionssss = collection.mintFunction.split("::")

        // const objectChanges = events.map((tx)=>tx.objectChanges)
        const createdObj :any = []
        let filterEvents:any=[]
        
        events.map((event)=>{
            if (event.effects.status.status ==="success" //&& event.objectChanges 
            && event.transaction?.data?.transaction?.["transactions"]
            && event.transaction?.data?.transaction?.["transactions"][1]
            && event.transaction?.data?.transaction?.["transactions"][1]["MoveCall"]
            && mintFunctionssss.includes(event.transaction?.data?.transaction?.["transactions"][1]["MoveCall"]["function"])
            // && event.transaction?.data?.transaction?.["transactions"][1]["MoveCall"]["function"]===collection.mintFunction
            ){             
                for (let i = 0; i < event.effects.created.length; i++) {
                  const element = event.effects.created[i].owner["AddressOwner"];
                  if (element) { 
                    createdObj.push({...event.effects.created[i],timestamp:+event.timestampMs})
                  }
                }
                // event.effects.created.map((obj)=>{
                //     if (obj.owner.AddressOwner ){
                //       // ||obj.type === "created" && obj.objectType.startsWith(collection.objectType)
                //         createdObj.push({...obj,timestamp:event.timestampMs})
                //     }                    
                // })
            }
        })
        // console.log(2222222222222, createdObj);
        // return 0

        for (let element of createdObj){
            const data = await client.getObject({
                id: element.reference.objectId,
                options: {
                    showType: true,
                    showOwner: true,
                    showPreviousTransaction: true,
                    showDisplay: true,
                    showContent: true,
                    showBcs: false,
                    showStorageRebate: true,
                }})
                const item = {...data.data,collectionAddress:address, timestamp:element.timestamp}
            //  console.log(11111111111111,item);
             
                filterEvents.push(item)
                // if (data ) {
                //     filterEvents.push({...data.data,collectionAddress:address, timestamp:element.timestamp})
                // }
        }

        // filterEvents = await createdObj.map(async(obj:any)=>{
        //     const data = await provider.getObject({
        //         id: obj.objectId,
        //         options: {
        //             showType: true,
        //             showOwner: true,
        //             showPreviousTransaction: true,
        //             showDisplay: true,
        //             showContent: true,
        //             showBcs: false,
        //             showStorageRebate: true,
        //         }})
             
        //     return ({...data.data,collectionAddress:address, timestamp:obj.timestamp})
        // })

        console.log('-------------------------------------------', filterEvents);
        
        
        nextCursor = {
            txDigest: eventLogs.nextCursor,
            eventSeq: 0,
        };
       // console.log('data',filterEvents.length)
        //return


        const latestProcessedTimestamp = events.length === 0 ? NaN : Number(events[events.length - 1].timestampMs);

        if (filterEvents.length ) await this.getOptions().onEventLogCrawled(this, filterEvents, nextCursor?.txDigest, Number(nextCursor?.eventSeq), Number(latestProcessedTimestamp));

        // update latest blocknumber fetched
        if (Number(latestProcessedTimestamp) > 0) 
        {
          let crawlStatus = await getRepository(CrawlStatus).findOne({ contractName: 'collection', contractAddress: address });
          if (crawlStatus) {
            crawlStatus.blockTimestamp = Number(latestProcessedTimestamp);
            if (nextCursor?.txDigest ) {
              crawlStatus.txDigest = nextCursor?.txDigest;
              crawlStatus.eventSeq = Number(nextCursor?.eventSeq);
            }
            await getRepository(CrawlStatus).save(crawlStatus);
          } else {
            crawlStatus = new CrawlStatus();
            crawlStatus.contractName = 'collection';
            crawlStatus.contractAddress = address;
            crawlStatus.blockTimestamp = Number(latestProcessedTimestamp);
            if (nextCursor?.txDigest ) {
              crawlStatus.txDigest = nextCursor?.txDigest;
              crawlStatus.eventSeq = Number(nextCursor?.eventSeq);
            }
            await getConnection()
              .createQueryBuilder()
              .insert()
              .into(CrawlStatus)
              .values(crawlStatus)
              .execute();
          }
        }

        return latestProcessedTimestamp;

      }));


      return {
        tx: '',
        seq: NaN,
        latestProcessedTimestamp: _.min(processedTime)
      };

    } catch (e) {
      console.log(e);
      const text=`--Product-run6-Partner--${this.getOptions().networkConfig.WEB3_API_URL}\n${e}`
      // await axios.get<any>(
      //     `https://api.telegram.org/bot5854879025:AAFw3S6c2A9sX8Auy4G5mJQ3Ed0xZMxTpWw/sendMessage?chat_id=-859929765&text=${text}`,
      //     {
      //       headers: {
      //         Accept: 'application/json',
      //       },
      //     },
      //   );
    }
  }

  @implement
  protected async getBlockCount(): Promise<number> {
    
    return null
  }
}
