import { JobManageEntity } from './entities';
import { getConnection, getRepository } from 'typeorm';
import pm2 from 'pm2';
import { CrawlJobStatus } from './Utils';
import { prepareEnvironment } from './prepareEnvironment';
import { join } from 'path';
const dotenv = require('dotenv');
dotenv.config();

const networkConfig = require(`./configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts.collection;

async function start() {
  const notRunCollections = await getRepository(JobManageEntity).find({
    where: {
      status: CrawlJobStatus.NOT_START,
      contractName: 'collection',
      type:'internal'
    },
    take: +contractConfig.NUMBER_CRAWLER_COLLECTION,
  });

  if (notRunCollections.length === 0) {
    return;
  }

  const ranCollections = await getRepository(JobManageEntity).count({
    where: {
      status: CrawlJobStatus.START,
      contractName: 'collection',
      type:'internal'
    },
  });

  const jobNumber = Math.ceil(ranCollections / +contractConfig.NUMBER_CRAWLER_COLLECTION);

  const newJobNumber = Math.ceil(
    (ranCollections + notRunCollections.length) / +contractConfig.NUMBER_CRAWLER_COLLECTION
  );

  console.log(jobNumber, newJobNumber);

  if (jobNumber === newJobNumber) {
    await Promise.all(
      notRunCollections.map(collection => {
        delete collection.updatedAt;

        collection.status = CrawlJobStatus.START;
        collection.jobName = `---Product---Collection cralwer page ${jobNumber}`;
        return getConnection()
          .createQueryBuilder()
          .update(JobManageEntity)
          .set(collection)
          .where('id = :id', {
            id: collection.id,
          })
          .execute();
      })
    );
    return;
  }

  await Promise.all(
    notRunCollections.map(collection => {
      delete collection.updatedAt;
      collection.status = CrawlJobStatus.START;
      collection.jobName = `---Product---Collection cralwer page ${newJobNumber}`;
      return getConnection()
        .createQueryBuilder()
        .update(JobManageEntity)
        .set(collection)
        .where('id = :id', {
          id: collection.id,
        })
        .execute();
    })
  );

  pm2.start(
    {
      script: join('./CollectionsEventLogCrawler.js'),
      name: `---Product---Collection cralwer page ${newJobNumber}`,
      args: [String(newJobNumber)],
    },
    err => {
      console.log(err);
      process.exit(1);
    }
  );

  return;
}

prepareEnvironment()
  .then(start)
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
