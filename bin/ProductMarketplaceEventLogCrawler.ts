import { getRepository, getConnection, EntityManager } from "typeorm";
import {
    CrawlStatus,
    NftEntity,
    UserWalletEntity,
    CollectionActivityEntity,
    OfferSaleEntity,
    JobManageEntity,
    CollectionOfferEntity,
} from "./entities";
import { IEventLogCrawlerOptions, BaseEventLogCrawler } from "./BaseEventLogCrawler";
import { prepareEnvironment } from "./prepareEnvironment";
import {
    CollectionActivityTypeEnum,
    CollectionTypeEnum,
    CrawlJobStatus,
    NetworkTypeEnum,
    OfferSaleStatusEnum,
    SaleStatusEnum,
    SaleTypeEnum,
} from "./Utils";
import { CollectionEntity } from "./entities/Collection";
import { ProductEventLogCrawlerByWeb3 } from "./ProductEventLogCrawlerByWeb3";
import { SuiClient } from "@mysten/sui.js/client";

const contractName = "marketv1";
const networkConfig = require(`./configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts[contractName];

// Use in making contract log entity to insert DB

prepareEnvironment()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

function start(): void {
    console.log(`Start crawling event logs of contract: ${contractName}`);

    const crawlerOpts: IEventLogCrawlerOptions = {
        onEventLogCrawled,
        getLatestCrawledBlockNumber,
        networkConfig,
        contractConfig,
    };

    const crawler = new ProductEventLogCrawlerByWeb3(crawlerOpts);
    crawler.start();
}

async function onEventLogCrawled(
    crawler: BaseEventLogCrawler,
    eventLogs: any,
    txId: string,
    eventSeq: number,
    lastProcessedTimestamp: number
): Promise<void> {
    for (const eventLog of eventLogs) {
        await getConnection().manager.transaction("SERIALIZABLE", async (manager) => {
            console.log("LOG EVENT-TRANSACTION", eventLog);
            const eventType = eventLog.type.split("::")[2];

            switch (eventType) {
                case "EventList": {
                    await listingNft(eventLog, manager);
                    break;
                }
                case "EventUpdateList": {
                    await updateList(eventLog, manager);
                    break;
                }
                case "EventDeList": {
                    await deleteListNft(eventLog, manager);
                    break;
                }
                case "EventBuy": {
                    await buyNft(eventLog, manager);
                    break;
                }
                case "EventOrder": {
                    await orderNft(eventLog, manager);
                    break;
                }
                case "EventCancelOrder": {
                    await cancelOrderNft(eventLog, manager);
                    break;
                }
                case "EventAcceptOffer": {
                    await acceptOffer(eventLog, manager);
                    break;
                }
                case "EventCollectionOrderV2": {
                    await makeCollectionOffer(eventLog, manager);
                    break;
                }
                case "EventCollectionCancelV2": {
                    await cancelCollectionOffer(eventLog, manager);
                    break;
                }
                case "EventCollectionAcceptOfferV2": {
                    await acceptCollectionOffer(eventLog, manager);
                    break;
                }
            }
        });
    }

   
}

async function getLatestCrawledBlockNumber(): Promise<{
    txId: string;
    eventSeq: number;
    timestamp: number;
}> {
    const log = await getRepository(CrawlStatus).findOne({
        contractName,
        contractAddress: contractConfig.CONTRACT_ADDRESS,
    });
    if (!log) {
        return {
            txId: "",
            eventSeq: NaN,
            timestamp: NaN,
        };
    }
    return {
        txId: log.txDigest,
        eventSeq: log.eventSeq,
        timestamp: log.blockTimestamp,
    };
}

async function listingNft(eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const client = new SuiClient({url:'https://fullnode.mainnet.sui.io/'|| this.getOptions().networkConfig.WEB3_API_URL.toString()})
    const time = +eventLog.timestampMs;
    let nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });
    let collection;
    if (!nft) {
        const collectionData = (await client.getObject({
            id: event.parsedJson.object_id,
            options: {
                showType: true,
                showOwner: true,
                showPreviousTransaction: true,
                showDisplay: true,
                showContent: true,
                showBcs: false,
                showStorageRebate: true,
            },
        })) as any;
        const address = collectionData.data.type.split("::")[0];
        collection = await manager.findOne(CollectionEntity, {
            where: {
                address,
            },
        });

        if (!collection) {
            const newCollection = new CollectionEntity();
            newCollection.address = address;
            (newCollection.name = "Unverify contract"),
                (newCollection.networkType = NetworkTypeEnum.SUI);
            newCollection.creatorId = String(-1);
            newCollection.collectionType = CollectionTypeEnum.ERC721;
            newCollection.status = -1;

            await manager.save(newCollection);
        }

        collection = await manager.findOne(CollectionEntity, {
            where: {
                address,
            },
        });

        const newNft = new NftEntity();

        newNft.collection_address = address;
        newNft.collectionId = collection?.id;
        newNft.nftId = event.parsedJson.object_id;
        newNft.title =
            collectionData.data.content.fields.name || collectionData.data.display.data.name;
        newNft.imageUrl =
            collectionData.data.content.fields.image_url ||
            collectionData.data.content.fields.img_url ||
            collectionData.data.display.data.image_url ||
            collectionData.data.display.data.img_url;
        newNft.description =
            collectionData.data.content.fields.description ||
            collectionData.data.display.data.description;
        newNft.blockTimestamp = eventLog.timestampMs;
        newNft.price = event.parsedJson.price;
        newNft.startPrice = event.parsedJson.price;
        newNft.endPrice = event.parsedJson.price;
        newNft.saleType = SaleTypeEnum.FIX_PRICE;
        newNft.nftStatus = SaleStatusEnum.LISTING;
        newNft.quantity = 1;
        newNft.owner_address = event.parsedJson.owner;
        newNft.listing_price = event.parsedJson.price;
        newNft.version = 1

        await manager.save(newNft);

        const crawlNft = await manager.findOne(JobManageEntity, {
            where: {
                contractAddress: address,
                contractName: "collection",
            },
        });

        if (!crawlNft) {
            const crawlStatus = new CrawlStatus();
            crawlStatus.contractName = "collection";
            crawlStatus.contractAddress = address;

            const crawlManager = new JobManageEntity();
            crawlManager.contractAddress = address;
            crawlManager.contractName = "collection";
            crawlManager.status = CrawlJobStatus.NOT_START;

            await manager.save(crawlManager);
            await manager.save(crawlStatus);
        }

      
    } else if (time > nft.blockTimestamp) {
        nft.blockTimestamp = eventLog.timestampMs;
        nft.price = event.parsedJson.price;
        nft.startPrice = event.parsedJson.price;
        nft.endPrice = event.parsedJson.price;
        nft.saleType = SaleTypeEnum.FIX_PRICE;
        nft.nftStatus = SaleStatusEnum.LISTING;
        nft.listing_price = event.parsedJson.price;
        nft.quantity = 1;
        nft.owner_address = event.parsedJson.owner;
        nft.updatedAt = new Date();
        nft.version =1
        if (!nft.imageUrl) nft.imageUrl = event.parsedJson.image_url;
        await manager.save(nft);
    }

    nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

   
    collection = await manager.findOne(CollectionEntity, {
        where: {
            address: nft.collection_address,
        },
    });

    const activity = new CollectionActivityEntity();
    activity.nftAddress = nft.nftId;
    activity.collectionId = collection?.id;
    activity.collectionAddress = collection?.address;
    activity.transactionId = eventLog?.id.txDigest;
    activity.timestamp = time;
    activity.blockTimestamp = time;
    activity.fromAddress = event.parsedJson.owner;
    activity.userAddress = event.parsedJson.owner;
    activity.price = +event.parsedJson.price;
    activity.activity = CollectionActivityTypeEnum.LISTING;

    await manager.save(activity);

    //await emitNoti("EventList", event);
}

async function updateList(eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

    if (!nft) return;

    const collection = await manager.findOne(CollectionEntity, {
        where: {
            address: nft.collection_address,
        },
    });

    if (time > nft.blockTimestamp) {
        nft.price = event.parsedJson.price;
        nft.listing_price = event.parsedJson.price;
        nft.startPrice = event.parsedJson.price;
        nft.endPrice = event.parsedJson.price;
        nft.updatedAt = new Date();
    }
    const activity = new CollectionActivityEntity();
    activity.nftAddress = nft.nftId;
    activity.collectionId = collection?.id;
    activity.transactionId = eventLog?.id.txDigest;
    activity.timestamp = +eventLog.timestampMs;
    activity.blockTimestamp = +eventLog.timestampMs;
    activity.collectionAddress = collection?.address;
    activity.price = +event.parsedJson.price;
    activity.fromAddress = event.parsedJson.owner;
    activity.userAddress = event.parsedJson.owner;
    activity.activity = CollectionActivityTypeEnum.UPDATE;

    await manager.save(activity);
    await manager.save(nft);
    //await emitNoti("EventUpdateList", event);
}

async function deleteListNft(eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

    if (!nft) {
        return;
    }

    if (time > nft.blockTimestamp) {
        nft.blockTimestamp = time;
        nft.price = null;
        nft.startPrice = null;
        nft.endPrice = null;
        nft.nftStatus = SaleStatusEnum.CANCEL;
        nft.updatedAt = new Date();
        await manager.save(nft);
    }
    const collection = await manager.findOne(CollectionEntity, {
        where: {
            address: nft.collection_address,
        },
    });

    const activity = new CollectionActivityEntity();
    activity.nftAddress = nft.nftId;
    activity.collectionId = collection?.id;
    activity.transactionId = eventLog?.id.txDigest;
    activity.timestamp = +eventLog.timestampMs;
    activity.blockTimestamp = +eventLog.timestampMs;
    activity.fromAddress = event.parsedJson.sender;
    activity.userAddress = event.parsedJson.owner;
    activity.collectionAddress = collection?.address;
    activity.activity = CollectionActivityTypeEnum.CANCEL;

    await manager.save(activity);

    //await emitNoti("EventDeList", event);
}

async function buyNft(eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

    if (!nft) {
        return;
    }

   

    const collection = await manager.findOne(CollectionEntity, {
        where: {
            address: nft.collection_address,
        },
    });

    const prevOwner = nft.owner_address;

    if (nft.blockTimestamp < time) {
        nft.owner_address = event.parsedJson.sender;
        nft.blockTimestamp = time;
        nft.price = null;
        nft.startPrice = null;
        nft.endPrice = null;
        nft.nftStatus = SaleStatusEnum.CANCEL;
        nft.market_price = String(+event.parsedJson.paid);
        nft.updatedAt = new Date();
    }

    const activity = new CollectionActivityEntity();
    activity.nftAddress = nft.nftId;
    activity.collectionId = collection?.id;
    activity.transactionId = eventLog?.id.txDigest;
    activity.price = +event.parsedJson.paid;
    activity.timestamp = time;
    activity.fromAddress = prevOwner;
    activity.userAddress = event.parsedJson.sender;
    activity.activity = CollectionActivityTypeEnum.COMPLETE;
    activity.collectionAddress = collection?.address;

    await manager.save(activity);
    await manager.save(nft);
    //await emitNoti("EventBuy", event);
}

async function orderNft(eventLog: any, manager: EntityManager) {
    const event = eventLog;

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

    if (!nft) {
        return;
    }

    if (+nft.offer_price < +event.parsedJson.price_order) {
        nft.offer_price = event.parsedJson.price_order;
        nft.updatedAt = new Date();
    }

    const collection = await manager.findOne(CollectionEntity, {
        where: {
            address: nft.collection_address,
        },
    });

    

    const newOfferSale = new OfferSaleEntity();
    newOfferSale.blockTimestamp = +eventLog.timestampMs;
    newOfferSale.expireTime = 0;
    newOfferSale.nftAddress = nft.nftId;
    newOfferSale.price = event.parsedJson.price_order;
    newOfferSale.quantity = 1;
    newOfferSale.status = OfferSaleStatusEnum.NOT_ACCEPTED;
    newOfferSale.userAddress = event.parsedJson.sender;
    newOfferSale.version = 1

    const activity = new CollectionActivityEntity();
    activity.nftAddress = nft.nftId;
    activity.collectionId = collection?.id;
    activity.transactionId = eventLog?.id.txDigest;
    activity.timestamp = +eventLog.timestampMs;
    activity.price = +event.parsedJson.price_order;
    activity.fromAddress = event.parsedJson.sender;
    activity.userAddress = event.parsedJson.owner;
    activity.activity = CollectionActivityTypeEnum.OFFER;
    activity.collectionAddress = collection?.address;

    await manager.save(activity);
    await manager.save(newOfferSale);
    await manager.save(nft);

    //await emitNoti("EventOrder", event);
}

async function cancelOrderNft(eventLog: any, manager: EntityManager) {
    const event = eventLog;

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

    if (!nft) {
        return;
    }

    const collection = await manager.findOne(CollectionEntity, {
        where: {
            address: nft.collection_address,
        },
    });

    const userWallet = await manager.findOne(UserWalletEntity, {
        where: { address: event.parsedJson.sender, networkType: NetworkTypeEnum.SUI },
    });

    if (!userWallet) {
        const newUserWallet = new UserWalletEntity();
        newUserWallet.address = event.parsedJson.sender;
        newUserWallet.networkType = NetworkTypeEnum.SUI;

        await manager.save(newUserWallet);
    }

    const offerSale = await manager.findOne(OfferSaleEntity, {
        where: {
            nftAddress: nft.nftId,
            userAddress: event.parsedJson.sender,
            price: event.parsedJson.price_order,
        },
    });
    if (offerSale) {
        await manager.remove(offerSale);
    }

    const activity = new CollectionActivityEntity();
    activity.nftAddress = nft.nftId;
    activity.collectionId = collection?.id;
    activity.collectionAddress = collection?.address;
    activity.transactionId = eventLog?.id.txDigest;
    activity.timestamp = +eventLog.timestampMs;
    activity.fromAddress = event.parsedJson.sender;
    activity.userAddress = event.parsedJson.owner;
    activity.blockTimestamp = +eventLog.timestampMs;
    activity.activity = CollectionActivityTypeEnum.CANCEL_OFFER;

    await manager.save(activity);

    //await emitNoti("EventCancelOrder", event);
}

async function acceptOffer(eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    const nft = await manager.findOne(NftEntity, { where: { nftId: event.parsedJson.object_id } });

    if (!nft) {
        return;
    }

    if (time > nft.blockTimestamp) {
        nft.blockTimestamp = time;
        nft.price = null;
        nft.startPrice = null;
        nft.endPrice = null;
        nft.nftStatus = SaleStatusEnum.CANCEL;
        nft.owner_address = event.parsedJson.sender;
        nft.market_price = event.parsedJson.price_order;
        nft.blockTimestamp = time;
        nft.updatedAt = new Date();
    }

    const offerSale = await manager.findOne(OfferSaleEntity, {
        where: {
            nftAddress: nft.nftId,
            userAddress: event.parsedJson.receiver,
            price: event.parsedJson.price_order,
        },
    });

    if (offerSale) {
        await manager.remove(offerSale);
    }

    const collection = await manager.findOne(CollectionEntity, {
        where: {
            address: nft.collection_address,
        },
    });

    const activity = new CollectionActivityEntity();
    activity.nftAddress = nft.nftId;
    activity.collectionId = collection?.id;
    activity.collectionAddress = collection?.address;
    activity.transactionId = eventLog?.id.txDigest;
    activity.price = +event.parsedJson.price_order;
    activity.timestamp = +eventLog.timestampMs;
    activity.fromAddress = event.parsedJson.sender;
    activity.userAddress = event.parsedJson.receiver;
    activity.activity = CollectionActivityTypeEnum.ACCEPT_OFFER;

    await manager.save(activity);
    await manager.save(nft);

    //await emitNoti("EventAcceptOffer", event);
}

async function makeCollectionOffer(eventLog: any, manager: EntityManager) {
    const event = eventLog;

    const collection = await manager.findOne(CollectionEntity, { where: { address: `0x${event.parsedJson.collection_id.name.split('::')[0]}` } });

    if (!collection) {
        return;
    }

    const checkOffer = await manager.findOne(CollectionOfferEntity, {
        where: {
            userAddress: event.parsedJson.sender_order, 
            collectionAddress: collection.address, 
            price: +event.parsedJson.price_order
        }
    })

    if (checkOffer) {
        checkOffer.quantity = +event.parsedJson.sum_quantity;
        await manager.save(checkOffer)
    } else {
        const collection_offer = new CollectionOfferEntity();
        collection_offer.userAddress = event.parsedJson.sender_order;
        collection_offer.collectionAddress = collection.address;
        collection_offer.price = +event.parsedJson.price_order;
        collection_offer.quantity = +event.parsedJson.sum_quantity;
        collection_offer.status = OfferSaleStatusEnum.NOT_ACCEPTED;
        collection_offer.blockTimestamp = +eventLog.timestampMs;
        await manager.save(collection_offer)
    }

    const activity = new CollectionActivityEntity();
    activity.collectionAddress = collection.address;
    activity.transactionId = eventLog.id.txDigest;
    activity.price = +event.parsedJson.price_order;
    activity.timestamp = +eventLog.timestampMs;
    activity.fromAddress = event.parsedJson.sender_order;
    activity.quantity = +event.parsedJson.quantity_order;
    activity.activity = CollectionActivityTypeEnum.COLLECTION_OFFER;
    await manager.save(activity);
    //await emitNoti("EventCollectionOrderV2", event);
}

async function cancelCollectionOffer(eventLog: any, manager: EntityManager) {
    const event = eventLog;

    const collection = await manager.findOne(CollectionEntity, { where: { address: event.parsedJson.collection_id.name.split('::')[0] } });

    if (!collection) {
        return;
    }

    const checkOffer = await manager.findOne(CollectionOfferEntity, {
        where: {
            userAddress: event.parsedJson.sender_order, 
            collectionAddress: collection.address, 
            price: +event.parsedJson.price_order
        }
    })

    if (checkOffer) {
        await manager.remove(checkOffer)
    }

    const activity = new CollectionActivityEntity();
    activity.collectionAddress = collection.address;
    activity.transactionId = eventLog.id.txDigest;
    activity.price = +event.parsedJson.price_order;
    activity.timestamp = +eventLog.timestampMs;
    activity.fromAddress = event.parsedJson.sender_order;
    activity.quantity = +event.parsedJson.quantity_order;
    activity.activity = CollectionActivityTypeEnum.CANCEL_COLLECTION_OFFER;
    await manager.save(activity);
    //await emitNoti("EventCollectionCancelV2", event);
}

async function acceptCollectionOffer(eventLog: any, manager: EntityManager) {
    const event = eventLog;

    const collection = await manager.findOne(CollectionEntity, { where: { address: event.parsedJson.collection_id.name.split('::')[0] } });

    if (!collection) {
        return;
    }

    const checkOffer = await manager.findOne(CollectionOfferEntity, {
        where: {
            userAddress: event.parsedJson.sender_order, 
            collectionAddress: collection.address, 
            price: +event.parsedJson.price_order
        }
    })

    if (+event.parsedJson.sum_quantity == 0 && checkOffer) {
        await manager.remove(checkOffer)
    } else {
        checkOffer.quantity = +event.parsedJson.sum_quantity
        await manager.save(checkOffer)
    }

    const activity = new CollectionActivityEntity();
    activity.collectionAddress = collection.address;
    activity.nftAddress = event.parsedJson.id_item
    activity.transactionId = eventLog.id.txDigest;
    activity.price = +event.parsedJson.price_order;
    activity.timestamp = +eventLog.timestampMs;
    activity.fromAddress = event.parsedJson.sender_accept;
    activity.userAddress = event.parsedJson.receiver_order;
    activity.quantity = 1;
    activity.activity = CollectionActivityTypeEnum.ACCEPT_COLLECTION_OFFER;
    await manager.save(activity);
    //await emitNoti("EventCollectionAcceptOfferV2", event);
}
