import { createConnection } from "typeorm";
import * as entities from "./entities";
import _ from "lodash";

import dotenv from "dotenv";
import * as Mongoose from "mongoose";
dotenv.config();

export async function prepareEnvironmentStake(): Promise<void> {
    const connectionString = process.env.DB_CONN_STRING_STAKE;
    console.log(`Application has been started.`);
    console.log(`Preparing DB connection...`);
    await createConnection({
        type: "postgres",
        host: process.env.TYPEORM_HOST,
        port: process.env.TYPEORM_PORT ? parseInt(process.env.TYPEORM_PORT, 10) : 3306,
        username: process.env.TYPEORM_USERNAME,
        password: process.env.TYPEORM_PASSWORD,
        database: process.env.TYPEORM_DATABASE,
        synchronize: false,
        logging: process.env.TYPEORM_LOGGING ? process.env.TYPEORM_LOGGING === "true" : true,
        cache: process.env.TYPEORM_CACHE ? process.env.TYPEORM_CACHE === "true" : true,
        entities: Object.values(entities),
    });

    await Mongoose.connect(connectionString);

    console.log(`Environment has been setup successfully...`);
    return;
}
