
import { MongoClient } from "mongodb";
import dotenv from 'dotenv'
dotenv.config()
const url = process.env.DB_CONN_STRING_STAKE || 'mongodb://tocen_stake:Ys72fCzb6S9OGKNiw5hbS33uClS1s6@192.168.20.14:27017,192.168.20.140:27017/tocen_stake?replicaSet=blockchain&authMechanism=SCRAM-SHA-1&authSource=tocen_stake';

const db = "tocen_stake";

const mongoClientDAO = new MongoClient(url);

const userStakeCollectionDAO = mongoClientDAO.db(db).collection("userStake");
const accountPoolNFTCollectionDAO = mongoClientDAO.db(db).collection("AccountPoolNFT");

  export class UserStakeDAO {
    constructor() {}
  
    async checkKeyStake(address) {
      try {
        const keyStake = await accountPoolNFTCollectionDAO.findOne({
          address: address,
        });
        if (!keyStake) {
          return 1;
        }
        switch (keyStake.nftLevel) {
          case 1:
            return 1.2;
          case 2:
            return 1.15;
          case 3:
            return 1.1;
          case 4:
            return 1.05;
        }
      } catch (error) {}
    }
  
    async getAllUserStake() {
      try {
        const res = await userStakeCollectionDAO.find().toArray();
        return res;
      } catch (error) {
        console.log(error);
      }
    }
 
    async updateDataUserStake(dataUpdate, session, mongoClient) {
      const userStakeCollection = mongoClient.db(db).collection("userStake");
      try {
        const res = await userStakeCollection.findOne({
          address: dataUpdate.address,
          pool_id: dataUpdate.pool_id,
        });
        const dataSave = { ...dataUpdate };
  
        if (res) {
          if (dataUpdate.timestamp_stake != res.timestamp_stake) {
            const date_now = new Date();
            const date_onchain = new Date(Number(dataUpdate.timestamp_stake));
            if (
              date_onchain.toLocaleDateString() != date_now.toLocaleDateString()
            ) {
              dataSave.balance_stake =
                Number(dataUpdate.balance_stake) +
                Number(res?.balance_stake ? res?.balance_stake : 0);
            } else {
              dataSave.balance_stake = res.balance_stake;
              dataSave.balance_pending =
                Number(dataUpdate.balance_stake) +
                Number(res.balance_pending ? res.balance_pending : 0);
            }
            await userStakeCollection.updateOne(
              { address: dataSave.address, pool_id: dataUpdate.pool_id },
              { $set: dataSave },
              { upsert: true, session }
            );
          }
        } else {
          dataSave.balance_pending = dataUpdate.balance_stake;
          dataSave.balance_stake = 0;
          dataSave.balance_interest = 0;
          dataSave.total_interest = 0;
          dataSave.claim_interest = 0;
  
          await userStakeCollection.updateOne(
            { address: dataSave.address, pool_id: dataUpdate.pool_id },
            { $set: dataSave },
            { upsert: true, session }
          );
        }
  
        return res;
      } catch (ex) {
        console.log(ex);
        throw new Error("Error updateDataUserStake");
      }
    }
  
    async updateDataUserClaim(dataUpdate, session, mongoClient) {
      const userStakeCollection = mongoClient.db(db).collection("userStake");
      const poolStakeCollection = mongoClient.db(db).collection("poolStake");
      try {
        const pool = await poolStakeCollection.findOne({
          pool_id: dataUpdate.pool_id
        })
        const res = await userStakeCollection.findOne({
          address: dataUpdate.address,
          pool_id: dataUpdate.pool_id,
        });
        if (res.timestamp_claim < dataUpdate.timestamp_claim) {
          const dataSave = { ...dataUpdate };
          if (pool.type != 'flexible') {
            dataSave.info_user = ''
          }
          const balance_pending =
            Number(res.balance_pending ? res.balance_pending : 0) -
            Number(dataUpdate.balance_claim);
          if (balance_pending < 0) {
            dataSave.balance_stake =
              Number(res.balance_stake) +
              Number(res.balance_interest || 0) +
              balance_pending;
            dataSave.balance_pending = 0;
            dataSave.balance_interest = 0;
          } else {
            dataSave.balance_stake =
              Number(res.balance_stake) + Number(res.balance_interest || 0);
            dataSave.balance_pending = balance_pending;
            dataSave.balance_interest = 0;
          }
          dataSave.balance_claim =
            Number(res.balance_claim ? res.balance_claim : 0) +
            Number(dataUpdate.balance_claim);
          dataSave.claim_interest =
            Number(res.claim_interest ? res.claim_interest : 0) +
            Number(dataUpdate.claim_interest);
          await userStakeCollection.updateOne(
            { address: dataSave.address, pool_id: dataUpdate.pool_id },
            { $set: dataSave },
            { upsert: true, session }
          );
    
          return await userStakeCollection.findOne({
            address: dataUpdate.address,
            pool_id: dataUpdate.pool_id,
          });
        }
        return res;
      } catch (ex) {
        console.log(ex);
        throw new Error("Error updateDataUserClaim");
      }
    }
  
    async createHistoryStake(data, session, mongoClient) {
      const historyStakePoolCollection = mongoClient.db(db).collection("HistoryStakePool");
      const HistoryCalculateInterestCollection = mongoClient.db(db).collection("HistoryCalculateInterestCollection");
      try {
        if (data.event != 'calculateInterest') {
          const res = await historyStakePoolCollection.findOne({ txId: data.txId });
          if (!res) {
            return await historyStakePoolCollection.insertOne(data, {session});
          }
          return res;
        } else {
          const res = await HistoryCalculateInterestCollection.findOne({ 
            address: data.address,
            pool_id: data.pool_id, 
            timestamp: data.timestamp 
          });
          if (!res) {
            return await HistoryCalculateInterestCollection.insertOne(data, {session});
          } 
          return res;
        }
      } catch (ex) {
        console.log(ex);
        throw new Error(ex);
      }
    }
  }
    