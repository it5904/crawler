import { MongoClient } from "mongodb";
import { UserStakeDAO } from "./stakePoolDAO";
import dotenv from "dotenv";
dotenv.config();
const url =
    process.env.DB_CONN_STRING_STAKE ||
    "mongodb://tocen_stake:Ys72fCzb6S9OGKNiw5hbS33uClS1s6@192.168.20.14:27017,192.168.20.140:27017/tocen_stake?replicaSet=blockchain&authMechanism=SCRAM-SHA-1&authSource=tocen_stake";

const db = "tocen_stake";

const mongoClient = new MongoClient(url);

const userStakeCollection = mongoClient.db(db).collection("userStake");
const poolStakeCollection = mongoClient.db(db).collection("poolStake");
const crawlStatusCollection = mongoClient.db(db).collection("CrawlStatus");

const getPool = async () => {
    const res = await poolStakeCollection.find().toArray();
    return res;
};

const calculateInterest = async () => {
    const checkCrawl = await crawlStatusCollection.findOne({
        module: "calculate_interest",
    });
    if (Date.now() - Number(checkCrawl.start_time) < 86400000) {
        return true;
    } else {
        const poolData = await getPool();
        const userStake = await UserStakeDAO.prototype.getAllUserStake();
        const session = mongoClient.startSession();
        try {
            await crawlStatusCollection.updateOne(
                { module: "calculate_interest" },
                { $set: { status: "running", start_time: Date.now() } },
                { upsert: true }
            );
            session.startTransaction();
            for (const userData of userStake) {
                const keyBoost = await UserStakeDAO.prototype.checkKeyStake(
                    userData.address
                );
                const pool = poolData.find(
                    (it) => it.pool_id == userData.pool_id
                );
                const apr = pool.apr / 100 / 365;
                const interest =
                    (Number(userData.balance_stake) +
                        Number(userData.balance_interest || 0)) *
                    keyBoost *
                    apr;
                const stake =
                    Number(userData.balance_stake) +
                    Number(userData.balance_pending);
                await UserStakeDAO.prototype.createHistoryStake(
                    {
                        address: userData.address,
                        pool_id: userData.pool_id,
                        timestamp: Date.now(),
                        apr: pool.apr,
                        apy: apr,
                        interest: Math.floor(interest),
                        balance_stake: stake,
                        key_boost: keyBoost,
                        event: "calculateInterest",
                    },
                    session,
                    mongoClient
                );
                await userStakeCollection.findOneAndUpdate(
                    { address: userData.address, pool_id: userData.pool_id },
                    {
                        $set: {
                            balance_interest:
                                Math.floor(interest) +
                                Number(userData.balance_interest || 0),
                            balance_stake: stake,
                            balance_pending: 0,
                            total_interest:
                                Number(userData.total_interest || 0) +
                                Math.floor(interest),
                        },
                    },
                    { session }
                );
                await poolStakeCollection.findOneAndUpdate(
                    { pool_id: userData.pool_id },
                    {
                        $set: {
                            reward: Number(pool.reward) - Math.floor(interest),
                        },
                    },
                    { session }
                );
            }
            await session.commitTransaction();
            await crawlStatusCollection.updateOne(
                { module: "calculate_interest" },
                {
                    $set: {
                        status: "success",
                        end_time: Date.now(),
                        messageError: "",
                    },
                }
            );
            console.log("calculateInterest successfully.");
        } catch (error) {
            console.log(error);
            await crawlStatusCollection.updateOne(
                { module: "calculate_interest" },
                {
                    $set: {
                        status: "failed",
                        end_time: Date.now(),
                        messageError: error.message,
                    },
                }
            );
            await session.abortTransaction();
        } finally {
            await session.endSession();
        }
    }
};

calculateInterest();
