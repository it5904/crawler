import { MongoClient } from "mongodb";
import { Connection, JsonRpcProvider } from "@mysten/sui.js";
import { UserStakeDAO } from "./stakePoolDAO";
import dotenv from "dotenv";
dotenv.config();

async function start() {
    const url =
        process.env.DB_CONN_STRING_STAKE ||
        "mongodb://tocen_stake:Ys72fCzb6S9OGKNiw5hbS33uClS1s6@192.168.20.14:27017,192.168.20.140:27017/tocen_stake?replicaSet=blockchain&authMechanism=SCRAM-SHA-1&authSource=tocen_stake";

    const db = "tocen_stake";

    const mongoClient = new MongoClient(url);

    const poolStakeCollection = mongoClient.db(db).collection("poolStake");
    const crawlStatusCollection = mongoClient.db(db).collection("CrawlStatus");

    const fullnode = [
        "http://192.168.8.122:9000/",
        "https://rpc-mainnet.suiscan.xyz/",
        "https://fullnode.mainnet.sui.io/",
    ];

    const getDataOnchain = async () => {
        const checkRunningJob = await crawlStatusCollection.findOne({module: "calculate_interest", status: "running"})
        if (checkRunningJob) {
            return true;
        }
        const session = mongoClient.startSession();
        try {
            await session.startTransaction();
            const random = Math.floor(Math.random() * fullnode.length);
            const connection = new Connection({
                fullnode: fullnode[random],
            });
            const provider = new JsonRpcProvider(connection);

            const dataCrawl = await crawlStatusCollection.findOne({
                module: "tocen_stake_token",
            });

            const query = {
                MoveModule: {
                    package: dataCrawl.contractAddress,
                    module: dataCrawl.module,
                },
            };
            let cursor = null;

            if (dataCrawl.txDigest && dataCrawl.txDigest != "") {
                cursor = {
                    txDigest: dataCrawl.txDigest,
                    eventSeq: String(dataCrawl.eventSeq),
                };
            }

            const res = await provider.queryEvents({
                query: query,
                cursor: cursor,
                limit: 100,
                order: "ascending",
            });

            const events = res.data;

            for (const item of events) {
                switch (item.type.split("::")[2]) {
                    case "EventStakePool": {
                        console.log("Stake", item);
                        await UserStakeDAO.prototype.updateDataUserStake(
                            {
                                address: item.parsedJson.owner_stake,
                                balance_stake: +item.parsedJson.balance_stake,
                                timestamp_stake:
                                    item.parsedJson.timestamp_stake,
                                pool_id: item.parsedJson.object_id,
                                timestamp_claim:
                                    item.parsedJson.timestamp_claim,
                                info_user: item.parsedJson.info_user,
                            },
                            session,
                            mongoClient
                        );
                        await UserStakeDAO.prototype.createHistoryStake(
                            {
                                address: item.parsedJson.owner_stake,
                                balance: +item.parsedJson.balance_stake,
                                timestamp: item.parsedJson.timestamp_stake,
                                pool_id: item.parsedJson.object_id,
                                txId: item.id.txDigest,
                                event: "EventStakePool",
                            },
                            session,
                            mongoClient
                        );
                        break;
                    }
                    case "EventAddStakePool": {
                        console.log("AddStake", item);
                        await UserStakeDAO.prototype.updateDataUserStake(
                            {
                                address: item.parsedJson.owner_stake,
                                balance_stake: +item.parsedJson.balance_stake,
                                timestamp_stake:
                                    item.parsedJson.timestamp_stake,
                                pool_id: item.parsedJson.object_id,
                                timestamp_claim:
                                    item.parsedJson.timestamp_claim,
                                info_user: item.parsedJson.info_user,
                            },
                            session,
                            mongoClient
                        );
                        await UserStakeDAO.prototype.createHistoryStake(
                            {
                                address: item.parsedJson.owner_stake,
                                balance_stake: +item.parsedJson.balance_stake,
                                info_user: item.parsedJson.info_user,
                                timestamp: item.parsedJson.timestamp_stake,
                                pool_id: item.parsedJson.object_id,
                                txId: item.id.txDigest,
                                event: "EventAddStakePool",
                            },
                            session,
                            mongoClient
                        );
                        break;
                    }
                    case "EventClaimPool": {
                        console.log("Claim", item);
                        await UserStakeDAO.prototype.updateDataUserClaim(
                            {
                                address: item.parsedJson.owner_claim,
                                balance_claim: +item.parsedJson.balance_claim,
                                pool_id: item.parsedJson.object_id,
                                timestamp_claim:
                                    item.parsedJson.timestamp_claim,
                                balance_remaining: +item.parsedJson
                                    .balance_remaining,
                                claim_interest: +item.parsedJson
                                    .balance_interest,
                            },
                            session,
                            mongoClient
                        );
                        await UserStakeDAO.prototype.createHistoryStake(
                            {
                                address: item.parsedJson.owner_claim,
                                balance_claim: +item.parsedJson.balance_claim,
                                balance_remaining: +item.parsedJson
                                    .balance_remaining,
                                claim_interest: +item.parsedJson
                                    .balance_interest,
                                timestamp: item.parsedJson.timestamp_claim,
                                pool_id: item.parsedJson.object_id,
                                txId: item.id.txDigest,
                                event: "EventClaimPool",
                            },
                            session,
                            mongoClient
                        );
                        break;
                    }
                    default:
                        break;
                }
            }

            const dataCraw = {
                blockTimestamp:
                    events.length === 0
                        ? Date.now()
                        : Number(events[events.length - 1].timestampMs),
                txDigest: res.nextCursor.txDigest,
                eventSeq: res.nextCursor.eventSeq,
            };

            await crawlStatusCollection.findOneAndUpdate(
                { module: "tocen_stake_token" },
                { $set: dataCraw },
                { session }
            );

            await session.commitTransaction();
            console.log("Craw data onchain successfully.");
        } catch (error) {
            console.log(error);
            await session.abortTransaction();
        } finally {
            await session.endSession();
        }
    };

    const getPoolDataOnchain = async () => {
        const res = await poolStakeCollection.find().toArray();
        return await Promise.all(res.map(async (it) => handlePoolOnchain(it)));
    };

    const handlePoolOnchain = async (item) => {
        const random = Math.floor(Math.random() * fullnode.length);
        const connection = new Connection({
            fullnode: fullnode[random],
        });
        const provider = new JsonRpcProvider(connection);
        const dataOnchain = await provider.getObject({
            id: item.pool_id,
            options: {
                showContent: true,
            },
        });
        const total_interest =
            Number(item.init_reward) -
            Number(dataOnchain.data.content["fields"]["bal_rewards"]);
        const apr =
            (((Number(item.reward) - total_interest) * Math.pow(10, -9)) /
                (Number(dataOnchain.data.content["fields"]["total_stake"]) *
                    Math.pow(10, -9))) *
            100;
        const apy = Number(Math.pow(1 + apr / 100 / 365, 365) - 1);

        await poolStakeCollection.findOneAndUpdate(
            { pool_id: item.pool_id },
            {
                $set: {
                    bal_reward: Number(
                        dataOnchain.data.content["fields"]["bal_rewards"]
                    ),
                    total_stake: Number(
                        dataOnchain.data.content["fields"]["total_stake"]
                    ),
                    total_interest:
                        Number(item.init_reward) -
                        Number(
                            dataOnchain.data.content["fields"]["bal_rewards"]
                        ),
                    apr: apr > 1400 ? 1400 : apr,
                    apy: apy > 10000 ? 1000000 : apy * 100,
                },
            }
        );
    };

    await getDataOnchain();
    await getPoolDataOnchain();
}
start();
