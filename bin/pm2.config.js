module.exports = {
    apps: [
        {
            name: "---Product---job manager collection",
            script: "./CronJob.js",
            env_production: {
                NODE_ENV: "production",
            },
            env_development: {
                NODE_ENV: "development",
            },
            watch: true,
            log_date_format: "YYYY-MM-DD HH:mm Z",
            cron_restart: "*/5 * * * *",
        },

        {
            name: "---Product---job add referral code",
            script: "./addReferralCode.js",
            env_production: {
                NODE_ENV: "production",
            },
            env_development: {
                NODE_ENV: "development",
            },
            watch: true,
            log_date_format: "YYYY-MM-DD HH:mm Z",
            cron_restart: "* * * * *",
        },

        // {
        //     name: "---Product---job manager IDO",
        //     script: "./IDO/IDOEventLogCrawler.js",
        //     env_production: {
        //         NODE_ENV: "production",
        //     },
        //     env_development: {
        //         NODE_ENV: "development",
        //     },
        //     watch: true,
        //     log_date_format: "YYYY-MM-DD HH:mm Z",
        //     cron_restart: "* * * * *",
        // },

        // {
        //     name: "---Product---job Claim IDO",
        //     script: "./IDOClaim/ClaimIDOEventLogCrawler.js",
        //     env_production: {
        //         NODE_ENV: "production",
        //     },
        //     env_development: {
        //         NODE_ENV: "development",
        //     },
        //     watch: true,
        //     log_date_format: "YYYY-MM-DD HH:mm Z",
        //     cron_restart: "* * * * *",
        // },

        {
            name: "---Product---job manager collection partner",
            script: "./CronJobPartner.js",
            env_production: {
                NODE_ENV: "production",
            },
            env_development: {
                NODE_ENV: "development",
            },
            watch: true,
            log_date_format: "YYYY-MM-DD HH:mm Z",
            cron_restart: "*/5 * * * *",
        },

        // {
        //     name: "---Product---job Stake",
        //     script: "./Stake/StakeEventLogCrawler.js",
        //     env_production: {
        //         NODE_ENV: "production",
        //     },
        //     env_development: {
        //         NODE_ENV: "development",
        //     },
        //     watch: true,
        //     log_date_format: "YYYY-MM-DD HH:mm Z",
        //     cron_restart: "*/10 * * * * *",
        // },

        {
            name: "---Product---job manager collection external",
            script: "./CronJobExternal.js",
            env_production: {
                NODE_ENV: "production",
            },
            env_development: {
                NODE_ENV: "development",
            },
            watch: true,
            log_date_format: "YYYY-MM-DD HH:mm Z",
            cron_restart: "*/5 * * * *",
        },

        {
            name: "---Product---job sync INO",
            script: "./SyncINO.js",
            env_production: {
                NODE_ENV: "production",
            },
            env_development: {
                NODE_ENV: "development",
            },
            watch: true,
            log_date_format: "YYYY-MM-DD HH:mm Z",
            cron_restart: "*/1 * * * *",
        },

        // {
        //     name: "---Product---job update discord user",
        //     script: "./UpdateDiscordUser.js",
        //     env_production: {
        //         NODE_ENV: "production",
        //     },
        //     env_development: {
        //         NODE_ENV: "development",
        //     },
        //     watch: true,
        //     log_date_format: "YYYY-MM-DD HH:mm Z",
        //     cron_restart: "00 00 * * *",
        // },

        {
            name: "---Product---job check health chainUrl",
            script: "./CheckHealthChainUrl.js",
            env_production: {
                NODE_ENV: "production",
            },
            env_development: {
                NODE_ENV: "development",
            },
            watch: true,
            log_date_format: "YYYY-MM-DD HH:mm Z",
            cron_restart: "*/3 * * * *",
        },

        {
            name: "---Product---job update image_nft null",
            script: "./updateImgNft.js",
            env_production: {
                NODE_ENV: "production",
            },
            env_development: {
                NODE_ENV: "development",
            },
            watch: true,
            log_date_format: "YYYY-MM-DD HH:mm Z",
            cron_restart: "*/5 * * * *",
        },

        // {
        //     name: "---Product---job backup Stake Coin",
        //     script: "./exportCollection.js",
        //     env_production: {
        //         NODE_ENV: "production",
        //     },
        //     env_development: {
        //         NODE_ENV: "development",
        //     },
        //     watch: true,
        //     log_date_format: "YYYY-MM-DD HH:mm Z",
        //     cron_restart: "59 23 * * *",
        // },

        // {
        //     name: "---Product---job crawl data stake pool",
        //     script: "./StakePoolCrawler/StakePool.js",
        //     env_production: {
        //         NODE_ENV: "production",
        //     },
        //     env_development: {
        //         NODE_ENV: "development",
        //     },
        //     watch: true,
        //     log_date_format: "YYYY-MM-DD HH:mm Z",
        //     cron_restart: "*/30 * * * * *",
        // },

        // {
        //     name: "---Product---job calc interest stake",
        //     script: "./StakePoolCrawler/StakePoolInterest.js",
        //     env_production: {
        //         NODE_ENV: "production",
        //     },
        //     env_development: {
        //         NODE_ENV: "development",
        //     },
        //     watch: true,
        //     log_date_format: "YYYY-MM-DD HH:mm Z",
        //     cron_restart: "*/10 * * * *",
        // },
    ],
};
