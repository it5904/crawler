import { BaseIntervalWorker, getLogger, implement, override } from "sota-common";

const logger = getLogger("BaseEventLogCrawler");
import { getWeb3ProviderLink } from "./Utils";

// Store in-progress block
let LATEST_PROCESSED_TIMESTAMP = NaN;
let TxID: string;
let EVENTSEQ = NaN;

// Crawler options, usually are functions to handle project-related logic
// Something like getting and updating data to database, ...
export interface IEventLogCrawlerOptions {
    readonly onEventLogCrawled: (
        crawler: BaseEventLogCrawler,
        eventLogs: any,
        txId: string,
        eventSeq: number,
        lastBlockTimestamp: number
    ) => Promise<void>;
    readonly getLatestCrawledBlockNumber: () => Promise<{
        txId: string;
        eventSeq: number;
        timestamp: number;
    }>;
    readonly networkConfig: any;
    readonly contractConfig: any;
}

export abstract class BaseEventLogCrawler extends BaseIntervalWorker {
    protected readonly _options: IEventLogCrawlerOptions;

    constructor(options: IEventLogCrawlerOptions) {
        super();
        this._options = options;
    }

    public getOptions(): IEventLogCrawlerOptions {
        // TODO: Please remove when production
        if (process.env.NODE_ENV !== "development") {
            console.log("Production event log crawler.");
        } else {
            console.log("development event log crawler.");
        }

        return this._options;
    }

    @implement
    protected async prepare(): Promise<void> {
        // Do we need any preparation here yet?
        this.setProcessingTimeout(60000);
    }

    @implement
    protected async doProcess(): Promise<void> {
        // Firstly try to get latest time  from network
        // const latestNetworkTimestamp = await this.getBlockCount();
        this._options.networkConfig.WEB3_API_URL = await getWeb3ProviderLink();

        // And looking for the latest processed block in local
        let latestProcessedTimestamp = LATEST_PROCESSED_TIMESTAMP;
        let txId = TxID;
        let eventSeq = EVENTSEQ;

        // // If there's no data in-process, then try to find it from environment variable
        // if (!latestProcessedTimestamp && this.getOptions().contractConfig.FORCE_CRAWL_TIME) {
        //   latestProcessedTimestamp = parseInt(this.getOptions().contractConfig.FORCE_CRAWL_TIME, 10);
        // }

        // If still no data, use the callback in options to get the initial value for this process
        if (
            !latestProcessedTimestamp ||
            isNaN(latestProcessedTimestamp) ||
            !txId ||
            !eventSeq ||
            isNaN(eventSeq)
        ) {
            latestProcessedTimestamp = (await this.getOptions().getLatestCrawledBlockNumber())
                .timestamp;
            txId = (await this.getOptions().getLatestCrawledBlockNumber()).txId;
            eventSeq = (await this.getOptions().getLatestCrawledBlockNumber()).eventSeq;
        }
        // if (!latestProcessedTimestamp && this.getOptions().contractConfig.FIRST_CRAWL_BLOCK) {
        //   latestProcessedTimestamp = parseInt(this.getOptions().contractConfig.FIRST_CRAWL_BLOCK, 10);
        // }

        // If there's no data, just process from the begin
        if (!latestProcessedTimestamp || !txId || !eventSeq) {
            latestProcessedTimestamp = NaN;
            txId = null;
            eventSeq = NaN;
        }

        /**
         * Start with the next time of the latest processed one
         */
        const fromBlockTimestamp = latestProcessedTimestamp;

        /**
         * If crawled the newest block already
         * Wait for a period that is equal to average block time
         * Then try crawl again (hopefully new block will be available then)
         */

        /**
         * Actual crawl and process blocks
         * about 10 minutes timeout based on speed of gateway
         */
        const toBlockTimestamp = await this.processBlocks(txId, eventSeq);

        /**
         * Cache the latest processed block number
         * Do the loop again in the next tick
         */
        LATEST_PROCESSED_TIMESTAMP = toBlockTimestamp.latestProcessedTimestamp;
        TxID = toBlockTimestamp.tx;
        EVENTSEQ = toBlockTimestamp.seq;

        // Otherwise try to continue processing immediately
        this.setNextTickTimer(this.getBreakTimeAfterOneGo());

        return;
    }

    protected getEventInOneGo(): number {
        return parseInt(this.getOptions().contractConfig.BLOCK_NUM_IN_ONE_GO, 10);
    }
    protected getAverageBlockTime(): number {
        return parseInt(this.getOptions().networkConfig.AVERAGE_BLOCK_TIME, 10);
    }
    protected getBreakTimeAfterOneGo(): number {
        return parseInt(this.getOptions().contractConfig.BREAK_TIME_AFTER_ONE_GO, 10) || 1;
    }
    protected getRequiredConfirmation(): number {
        return parseInt(this.getOptions().networkConfig.REQUIRED_CONFIRMATION, 10);
    }

    protected abstract processBlocks(
        txId: string,
        eventSeq: number
    ): Promise<{ tx: string; seq: number; latestProcessedTimestamp: number }>;
    protected abstract getBlockCount(): Promise<number>;
}

export default BaseEventLogCrawler;
