import { getRepository, getConnection, EntityManager } from "typeorm";
import { CrawlStatus, NftEntity, CollectionEntity, CollectionActivityEntity } from "../entities";
import { IEventLogCrawlerOptions, BaseEventLogCrawler } from "../BaseEventLogCrawler";
import { IDOTocenEventLogCrawlerByWeb3 } from "./IDOTocenEventLogCrawlerByWeb3";
import { prepareEnvironmentStake } from "../prepareEnvironmentStake";
import { CollectionActivityTypeEnum, SaleStatusEnum } from "../Utils";
import { emitNoti } from "../utils/EmitNoti";
import AccountProjectModel, { AccountProjectSchema } from "../entities/AccountProject.mongo";
import CrawlStatusModel from "../entities/CrawlStatus.mongo";
const contractName = "collection";
const networkConfig = require(`../configs/${process.env.NODE_ENV}`);
const contractConfig = networkConfig.contracts[contractName];

prepareEnvironmentStake()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

function start(): void {
    console.log(`Start crawling event logs of contract: ${contractName}`);

    console.log(contractConfig);

    const crawlerOpts: IEventLogCrawlerOptions = {
        onEventLogCrawled,
        getLatestCrawledBlockNumber,
        networkConfig,
        contractConfig,
    };

    const crawler = new IDOTocenEventLogCrawlerByWeb3(crawlerOpts);
    crawler.start();
}

async function onEventLogCrawled(
    crawler: BaseEventLogCrawler,
    eventLogs: any,
    txId: string,
    eventSeq: number,
    lastProcessedTimestamp: number
): Promise<void> {
    for (const eventLog of eventLogs) {
        await getConnection().manager.transaction("SERIALIZABLE", async (manager) => {
            // console.log("LOG EVENT-TRANSACTION", eventLog);
            const eventType = eventLog.type.split("::")[2];

            switch (eventType) {
                case "BuyCoin": {
                    await buyCoin(eventLog, manager);
                    break;
                }
                // case job?.functionRefund:
                // case job?.functionClaim: {
                //     await claimCoin(nameProject,eventLog, manager);
                //     break;
                // }
            }
        });
    }
}

async function getLatestCrawledBlockNumber(): Promise<{
    txId: string;
    eventSeq: number;
    timestamp: number;
}> {
    // const log = await getRepository(CrawlStatus).findOne({ contractName });
    // if (!log) {
    return {
        txId: "",
        eventSeq: NaN,
        timestamp: NaN,
    };
    // }
    // return {
    //     txId: log.txDigest,
    //     eventSeq: log.eventSeq,
    //     timestamp: log.blockTimestamp,
    // };
}

async function buyCoin(eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;
    const nameProject = "tocen";

    AccountProjectSchema.add({
        [nameProject]: {
            type: Object,
            required: false,
            unique: false,
        },
    });


    const deposit = nameProject + `.deposit${event.parsedJson?.pool_deposit}`;
    const depositTime = nameProject + `.deposit${event.parsedJson?.pool_deposit}Time`;
    const depositTxnID = nameProject + `.deposit${event.parsedJson?.pool_deposit}TxnID`;

    const data = await AccountProjectModel.findOneAndUpdate(
        { address: event.parsedJson?.owner_buy || event.sender },
        {
            $set: {
                [deposit]: +event.parsedJson?.balance_sui,
                [depositTime]: time,
                [depositTxnID]: event.id.txDigest,
            },
        },
        { new: true, upsert: true }
    );
}

async function claimCoin(nameProject: string, eventLog: any, manager: EntityManager) {
    const event = eventLog;
    const time = +eventLog.timestampMs;

    AccountProjectSchema.add({
        [nameProject]: {
            type: Object,
            required: false,
            unique: false,
        },
    });

    const isClaim = nameProject + ".isClaim";
    const claimTime = nameProject + ".claimTime";
    const claimTxnID = nameProject + ".claimTxnID";

    const data = await AccountProjectModel.findOneAndUpdate(
        { address: event.transaction.data.sender },
        {
            $set: {
                [isClaim]: true,
                [claimTime]: time,
                [claimTxnID]: event.digest,
            },
        },
        { new: true, upsert: true }
    );
}
