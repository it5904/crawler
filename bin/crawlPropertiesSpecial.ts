import { getRepository } from "typeorm";
import { prepareEnvironment } from "./prepareEnvironment";
import {
    CollectionEntity,
    NftEntity,
    NftPropertiesEntity,
    TocenUserDiscordEntity,
} from "./entities";
import { Client, GatewayIntentBits } from "discord.js";
import pLimit from "p-limit";
import { getWeb3ProviderLink } from "./Utils";
import { Connection, JsonRpcProvider } from "@mysten/sui.js";
import { xor } from "lodash";
import axios from "axios";
import { readFileSync, writeFileSync } from "fs";
import { join } from "path";

prepareEnvironment()
    .then(start)
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });

function syncWriteFile(filename: string, data: any) {
    /**
     * flags:
     *  - w = Open file for reading and writing. File is created if not exists
     *  - a+ = Open file for reading and appending. The file is created if not exists
     */
    writeFileSync(join(__dirname, filename), data, {
        flag: "w",
    });

    const contents = readFileSync(join(__dirname, filename), "utf-8");
    console.log(contents); // 👉️ "One Two Three Four"

    return contents;
}

const data = [
    "0x37642e01961e6dad3b8e9c1856638bf25394681e84d139ced93da9190572bc44",
    "0x45d765201c48d19fb39bfbcab1c1be33cf91e67148ddd10988d3b420b869e399",
    "0xcb13a23535b65a64d569726d1a09503cf641b95d2ae1d4a5ba595ccadb891e6f",
    "0x02cdf5d69b3060eb9aeb716436812b07fa6c339bf57649994224c28efa8a39df",
    "0x7190935fc09bd242d2c733413455d6b91ba7da459ec9fd4cf778804f02358c0b",
    "0x9aca23aad2c4bad89ee85bbe4e43e02c5e58989c2ca5e4e2fac36097c3333331",
    "0x03c841a77e8b61257ae5eecf072ca66be746f92627439fadafd4c39f6664b2b5",
    "0x261d38281cd9a041d4e252bebc37ca1ad7fa8bb0baaa072996433f9ee7d48ac6",
    "0x579ddab0c2bb5f8dce5746ec5e45336e45f80b27b4768e406f49de1702c72ca2",
    "0x1365f7c079c575381a051494f2ef2bdbb9007cf8d6563a966bbd858aa2396bfe",
    "0x6e74fb811350c7eb6013b2e9ad437b97fe47f788fdab5fddb16daa6edc753e84",
    "0xcf7882dc43f69e809e6414b8df179e7d2b346700e86db0f6940136ff4a3079bb",
    "0x60deccde6dc7d5ee3370d77fdfaf0cac37bc70d42b5f7b5d5632dfaa7f15a52d",
    "0x7e0ca61ad147a02c540ad5abfab0dd0f8cc6e7d01a0d6d208247cbbebb686b20",
    "0xd361c734c2ca04aa3ae91f2f220a091f8e1aa165e7eca4a86ff262cf5ac0c491",
    "0x00bbc7bf9e68ae755c41cbab7ab542d52481e4d3d46736ba18e71f292a9686f7",
    "0x5132b25d22572c40c4c9f2cd1a08932d221f236b24ad361973906fd9e82ec821",
    "0xc5a13bd341fbab0e2b1f53db9704fffb79663924068e3f0feb01fae6c1b89922",
    "0x6c899e466fbada16375c45753f6e0690fb82f278ed8375d9cf393ae88f390477",
    "0xafc4dd63b9d27ddd415e148493494d0ab90d55ecd3514b92d909b96b1391e1ec",
    "0xc9b6817fadb8d0c983ce2b9829bc1773546b1e0aaabeb8214c1e8960c756ad8e",
    "0x1ba158ff5b4c37620ef77039f9bd1b329cbdf1d55a90f54f17ccf926bf5658e6",
    "0x1c5675cf34e833e0829c0e7ec3311b94f548578855f5f3ad61a1192dc0189b21",
    "0x80b1dee4614aa10a20d244ec82e217bb620449faf6ae6dda60b5cd81c74fb610",
    "0x0b2a4093916a956ae5c38a54ca4f293ecd92b35ab025e8f19e3133810662f789",
    "0xd1fddd7ecdbdc0d0ba319a13bf0fe954e46fa43ce29f330f61825a05662d9bee",
    "0xfafab94db4d63acecb66306544e1316870523bcdc6d34287ba40d99be019b263",
    "0x2c4d0ec72623f28dd5061b51f7cfb5000c216fa2c48d1a6670790f3e1a0149e1",
    "0x4cf80425e93e17713d96cab9582557e5e6b1c40c29eb7e2c9540f7380d70d5c4",
    "0xd99c8cc9a3478ad742020b8a576f726a67467a4262a6b4653d8fe09c66ca38e0",
    "0x01a84017a29040837a7974c23d7654dd164c97784811cced59d0dd9af245b6dd",
    "0x17fe9bf6c3f4fc40a3ab5ba294c6248a4f308ce5d834e5b2256ade369ebf67f8",
    "0x9018440836b88301d32c26be986c8cc264594e8c3bbaed9ccba2d77320e176ca",
    "0x76f826dac655b2009c0e86fcc91a426ac19784c3fd0c98541fd7efa624665b2c",
    "0x8122e4975b992468181f6034b07d6cee00eaff63fe84f15cc172e76e4a74f2da",
    "0xbd1a97b8d87472e75a4cd9a5e59ef7304665e2d10cd99e23f144345e925fddad",
    "0x02fe15905ed2ee87451c98c249d2e9d9b4cd4f31c8f2a1cc098e7c0b93262f18",
    "0x07a944986b8989dda27c30421be822df557d2ef5f7359853d9b7394ca87b8c2a",
    "0x9c1e7e92ff4b961e1ec2caa90043bffe25191e46319c6d1778ca286a96a3fd65",
    "0x10cbef01913868913c3cd5b9e631a79350a2230d17a13e4c2e5bdb3dfc3efe12",
    "0x2630e6bdfc42bc6dd85cd35a47deaa0d3b20ed4bcaf938f71576debcca6dee90",
    "0xa5c686ef83c3a80e360610fe09fddded868b30621f23b7bdf371411ad06ead39",
    "0x17882ac2103bad9945c49367ab10aaa6363c3f074bbde9dcff642b5877056c03",
    "0x1b723fad6529e9a993f3badf862c555f390e97ecc33af96760bf2d42561ede5c",
    "0x215decd26204bc3e09ac6d3a74cde5c1bb5d7ca4ed0dea5a11c140792a6f1230",
    "0x1f94fe5f58653e5138760d888bae8b340a990b761b89aa55885815c757248f8d",
    "0x4a9b5a5fea6f94e23584ce99c754d40868a7cde68ddbdfac73282483f4ebf2a1",
    "0xb2bdd2a93ad64b1af56e286cf725c135471570844d11fa18d3ea089329b3d455",
    "0x0002c84202d14d68678dde9a9f6127cbbf1cf09acedf3eac132ff956330b1fdf",
    "0x157cb5cfb3a71485f82af4ab323df77a2280d6f4991def483338370367b69ee0",
    "0x7e0e76937596df564a43db994f7530e3dc27d3f8633a4330c2264fccb65be538",
    "0x0e05aa9b5999c899c5cf930a1136dfbcf4532aef9812968f850f64dd234dddd7",
    "0x50fb0b8d16624268e9be7bc185ae7ff230e1abfdbb8b314847a9f9cc88d86659",
    "0x817eb221cce6730214809634b38eba88c858ef900368939b98788ebb1d05e531",
    "0x1202cea400dcc12c5dfa228d77d86a51b70baae08231a134e423fb42ccbcc5e5",
    "0x30fb3edb41ecdac1a75e5495e8135db7858f03d732f55d8e282a3448c0eb65b2",
    "0x7b13696c7330fa33a1b0c2acce903c7dd60b71373a0fc49cce364de836bc8584",
    "0x9269bc7754660d032daf8f710afff35caf42ac6b11eb063aab6aa621ab7bf188",
    "0xe010c3c11d8f8ee98948b38f9cdb38a43c5145727b4f854b1d42aa1834114550",
    "0xe76fba146a4e90d7ec81aab47afd8cebc5ed16a1c458380efc7f0adf15aadb78",
    "0x6aa370c595561d2f859b44897bc216cb2df049633a1234da51fbdd34e6c98e1f",
    "0x8abd5fc071a42c00defa15917516f69cfae9e1bbb6a96a8afced34eee0da7e08",
    "0xf96219b5ffd62f3d550637e0f70c7bba3033af76e1f60054a3c2a0ef865caff4",
    "0x600456015a29feda270901d3cbeb7a78d042359abc4171e06de916d0d285bae4",
    "0x66ba226c9e92a0fe13175d42dbabbf9b99a64e0a286ff73883d9c1e79992b2b2",
    "0xd336a9fe107b436b21823317e314eabb4ed9f671b995bc5f1729291cc082a0e2",
    "0x00bf0141ad5fff1a4402b45b44182f0498197ea73d01919ed8779e78bdce1564",
    "0x25c2cdaad214ec86e43d2e83e5e5c0a82d4d29ea2440508d7c175e931fcfac4f",
    "0x7cf0fadb3d75f2cceb64708edde5e6964edc37732505edaaf906fd79eb3f8f65",
    "0x2c5ba1739b65f40606d890551b746f86170def991753558c3ada7d009e361d18",
    "0xe8b52d17b90205d2b06732f5e57efeb785e9390a45d1f367577dc0cd3503fcc8",
    "0xef3fec9e0c278b33377a98a6656425171ef5446b08f405b178ed7e6f46e64fb6",
    "0x1f522255f3f39a2a5c630d3b5eaf2cfd746c93a83d5ecc8b901e7642e0f54754",
    "0x45ce2d8e0825eff586105feaec659432d60135ec6cdbc7722d55f4e7f1a8d22c",
    "0x7d5c2df06e3a678d13d39b90e4456b8335d7fd721746457c30a1bba3d173153b",
    "0x85f3648aff65d18d1789c106b2c0f08091726998bd4abf4ad50f7562b881dab4",
    "0xaec680d87bc1c06e867b1ccddee5be0c52121d19067d77889e7e270c636fd14a",
    "0xc1e4375cc968bc334bf341e8b75274dc23318237be7229b8120d78305f273615",
    "0x37be5fc2c2027839293689fdac1ae9ee75cbb05173422cef612126bbca09675a",
    "0x5dcd66a979a92f8d4a8a08de8cfc869d9c742d09df737959144ae7fc11857b75",
    "0x8d60b2f3624a15140a4026ef98b34335269002fecf4ef279f4fb4477f22edc8c",
    "0x034322c012563cdcff2f28a843e0ba21b56110cfcfe480364bd3870ae52f63e3",
    "0x444e061de3427ad8857003497a1f5befac0cefeb7d49f4939070410a8f2e7fc2",
    "0x4b824dbd0c974f7a0afef0ed4f8e20215803be2616466ec7b085260c5a1a6c6f",
    "0x54813f8d5740fa799f5995fe73bd6e99bcb57c28663d769861c1fc43de43759c",
    "0x75e847871dd058fd9f392118f19aa014162b63d697963ebe1c85589a4280fd6b",
    "0x761f115077446cfce2ca77c251b22be0e9f6ce5727220ede063e9925d397e090",
    "0x4e9ed012a2173b3c13e667e1f33f7d6d869efffb833faba913c6868df5ff12de",
    "0x6be8c6a39897fcde8f85e319c5af0f9108d2faf96e7018c272105bfaf43d6a98",
    "0xd65e9332bd1f236501e4d9f75166d3818a868a19c9970f3c3b65985da297f64b",
    "0x11f02b3684dc4b5e1f9d49bf5df92d67a9a34841048ae37bc1ce3e4054ebc14e",
    "0x726afbd1898039067eefabc1e4b96addebb39add4522086a81a4c3bf9e29162b",
    "0x93e14566f18ef6e64dce87328f02eca625dc80f0cfd845f09ba7eaeadbbbb3aa",
    "0x12cbb023a52913e13a71ebb038cda452ff636c8a151009e37bb8b6d81150567d",
    "0xd8d6df6465c92e24b41b8ef6c6a281495187cfafa2b79b6091d0ef6992e48428",
    "0xe1c3dbce70df1348d0241c51f7039468a4cb58f0a450f16b0aa58e8b306d8c0f",
    "0x8f036a598553b8a963aebf6c9278e635acd27fab2abff248c3d4c4fcfe94d694",
    "0xab6d385d99cab6a0d42c7d20cfedcfade219bfebf14767c97de6b163bc884833",
    "0xdff3a51900a79c43c3b7aa5ce23edd34c974fd8f1b3c803b1ae2e4c92326a985",
    "0x3fea3b2167cd830bc3bbdd577c20a8f9c091474faf2a6e137653e88bfbce875c",
    "0xc3c650fb3a05cdfe2f07241cb292af6a6668801fa1e58a859fc8e2f1e0d99446",
    "0xd0ade55b6f4853afd1a59cb1da6714076985d63e0ca0094b0256d59cec45bd08",
    "0x423dc16f54c4a1deb3119c011de5168c18927052db78aceeb4687f219dd48a83",
    "0x4ee9e94d9ab929487008af301e20c8e444fdc01f80d461223b153252d83d5557",
    "0x60d267eb2a8698c9c57c74062493bf1ab4d03e4f894160a6c99d90f361db37c5",
    "0x8c9e5e50196e8389859b45dd5e42a2b659b446720d2035dacd6a304145df883c",
    "0xcaf08cb296a546d4b0dfec207528514a26e1708e5cf2711962070d3abffcc272",
    "0xcecac12f19c65b0e97574cc1287d0b742fc02052ada9b2fea418d050acdd7c81",
    "0xa9e022ddbef93ed630717802ba79c6d05413e5c6f809a5bf2bac05fe65d6a7b2",
    "0xbe92122444bd65e9e49510d5eccc4f28187ed9574e551e01b7b215942504ff1c",
    "0xe6d9ecc8beb06c4d36eb1105cefd12873ba91145ce97c9209eeeb0514f587d20",
    "0x592b0b65a16e62e3d8c6d4ac4b89af12e87668dbcd285c1f2f75a669a96a81e9",
    "0x79c99287bc1089856d7915bb711360e9a8b116286c3829aa2715c57eaf7d0295",
    "0xc89d787dd7208b78ce7c478bb891022cc11f189f2a8e417f2ff5469b54556d7f",
    "0xca9bbb18ab99ad415a053e823def733e4bd32b3276a00e0f2a4208f998e4d57c",
    "0x1c418b2fc2f56ae6728eff922ab6cb3530355de1e283f92911a57e93dc278baf",
    "0x222630c28058fd229c0eea60d96fe633a8e259af6b341079b23df63ea9b84361",
    "0x980fc13ee8bc9cb5c76260ba278e3202a5dcdb364301fb3b2ae61d8158cbc725",
    "0x9bee2fc8df06bd187e053e79eca120915bc5c2fae113036c3f12b03bc29be5c5",
    "0x2dc638d591755754464673eb133f98dd5aeebf07b3cde14e95b3e6146c9674a3",
    "0x2e3bfa767ce51bfbf6af7a3493d3dbf71e4206c3a8cba82e3352527f53170e3d",
    "0xe524ab276e59877f28f675db3ce36240620cd4b5fab07325ca4863c27509b5ee",
];
let result = [];

async function test() {
    for (let i = 0; i < data.length; i++) {
        const temp = await getRepository(NftEntity)
            .createQueryBuilder("n")
            .select("n.nft_address, n.ranking")
            .where("n.nft_address = :nftAddress", { nftAddress: data[i] })
            .getRawOne();
        result.push(temp);
    }
    syncWriteFile("test.txt", JSON.stringify(result));
}

async function start() {
    const addressCollection = "0x31363086e130d0495bd41284c04d1b4700c08b67e448f068b9cab483a2f682f3";
    const connection = new Connection({
        fullnode: "http://192.168.8.122:9000/",
    });
    const provider = new JsonRpcProvider(connection);

    let hasNext = true;
    let cursor = null;
    const query = {
        MoveModule: {
            package: addressCollection,
            module: "boho_collection",
        },
    };

    while (hasNext) {
        const eventLogs = await provider.queryEvents({
            query: query,
            cursor: cursor,
            limit: 100,
            order: "ascending",
        });
        const data = eventLogs.data;
        cursor = eventLogs.nextCursor;
        hasNext = eventLogs.hasNextPage;

        for (let i = 0; i < data.length; i++) {
            const nft = data[i];
            if (nft.type.split("::")[2] !== "NFTMinted") continue;
            const nftInfo = await provider.getObject({
                id: nft.parsedJson.object_id,
                options: {
                    showDisplay: true,
                    showContent: true,
                },
            });

            const link = nftInfo.data.display.data["link"] as string;
            const fixLink = link.split(" ").join("");

            const res = await axios.get<Object, any>(fixLink, {
                headers: {
                    Accept: "application/json",
                },
            });
            const content = res.data.attributes;
            let properties: NftPropertiesEntity[] = [];

            if (content) {
                for (let j = 0; j < content.length; j++) {
                    let attributes = content[j];
                    const newPropertiy = new NftPropertiesEntity();
                    newPropertiy.nftAddress = nft.parsedJson.object_id;
                    newPropertiy.collectionAddress = addressCollection;
                    // newPropertiy.key = attributes.fields.key;
                    // newPropertiy.value = attributes.fields.value;
                    newPropertiy.key = attributes.trait_type;
                    newPropertiy.value = attributes.value;

                    properties.push(newPropertiy);
                }
                try {
                    await getRepository(NftPropertiesEntity).save(properties);
                } catch (error) {}
            }
        }
    }
}
