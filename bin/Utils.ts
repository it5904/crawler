import { getRepository } from "typeorm";
import { ChainUrlEntity } from "./entities";

export async function getWeb3ProviderLink(): Promise<string> {
    const data = await getRepository(ChainUrlEntity).find({
        where: {
            status: ChainUrlStatusEnum.GOOD,
        },
    });
    const chainUrls = data.map((x) => x.chainUrl);

    const randomElement = chainUrls[Math.floor(Math.random() * chainUrls.length)];
    return randomElement;
}

export async function getWeb3ProviderLinkProduct(): Promise<string> {
    const data = await getRepository(ChainUrlEntity).find({
        where: {
            status: ChainUrlStatusEnum.GOOD,
            env:"product"
        },
    });
    const chainUrls = data.map((x) => x.chainUrl);

    const randomElement = chainUrls[Math.floor(Math.random() * chainUrls.length)];
    return randomElement;
}

export enum NetworkTypeEnum {
    ALEO = 0,
    SUI = 1,
}

export enum CollectionActivityTypeEnum {
    LISTING,
    CANCEL,
    UPDATE,
    OFFER,
    CANCEL_OFFER,
    ACCEPT_OFFER,
    COMPLETE,
    TRANSFER,
    MINT,
    COLLECTION_OFFER,
    ACCEPT_COLLECTION_OFFER,
    CANCEL_COLLECTION_OFFER,
}

export enum CollectionTypeEnum {
    ERC721 = 0,
    ERC1155 = 1,
}

export enum SaleTypeEnum {
    FIX_PRICE,
    ENGLISH_AUCTION,
}

export enum CollectionStatusEnum {
    NOT_AVAILABLE = -1,
    UNVERIFIED,
    VERIFIED,
}

export enum SaleStatusEnum {
    MINTED = -1,
    LISTING,
    CANCEL,
    SOLD_OUT,
}

export enum OfferSaleStatusEnum {
    ACCEPTED,
    NOT_ACCEPTED,
    CANCEL,
}

export enum CrawlJobStatus {
    NOT_START,
    START,
}

export enum ChainUrlStatusEnum {
    GOOD,
    BAD,
}

export const ALLOCATION_CACULATION_QUEUE = "ALLOCATION_CACULATION_QUEUE";
export const FULL_DATE_TIME = "YYYY-MM-DD HH:mm:ss";
export const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";
