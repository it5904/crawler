build:
	yarn build && make copy

install:
	yarn

re-install:
	rm -rf node_modules/* yarn.lock package.lock.json
	yarn


test-ProjectLogsCrawler-worker:
	yarn dev-run

test-CreateAllocationEventLogCrawler-worker:
	yarn dev-run2


copy:
	cp  -r bin/configs/  dist/
	cp -r .env dist/
	cp -r bin/pm2.config.js dist/


docker:
	cp .env.development .env
	docker-compose up -d --build

